<?php

/* basel/template/common/cart.twig */
class __TwigTemplate_7dd990654980944a702eeda45acf1143d329a504647adb0893b91f984510d9d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "  <ul id=\"cart-content\">
    ";
        // line 2
        if (((isset($context["products"]) ? $context["products"] : null) || (isset($context["vouchers"]) ? $context["vouchers"] : null))) {
            // line 3
            echo "    <li>
      <table class=\"table products\">
        ";
            // line 5
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 6
                echo "        <tr>
          <td class=\"image\">
          ";
                // line 8
                if ($this->getAttribute($context["product"], "thumb", array())) {
                    // line 9
                    echo "            <a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" /></a>
            ";
                }
                // line 11
                echo "            </td>
          <td class=\"main\"><a class=\"product-name main-font\" href=\"";
                // line 12
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a>
            ";
                // line 13
                echo $this->getAttribute($context["product"], "quantity", array());
                echo " x <span class=\"price\">";
                echo $this->getAttribute($context["product"], "price", array());
                echo "</span>
            ";
                // line 14
                if ($this->getAttribute($context["product"], "option", array())) {
                    // line 15
                    echo "            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                        // line 16
                        echo "            <br />
            - <small>";
                        // line 17
                        echo $this->getAttribute($context["option"], "name", array());
                        echo " ";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "</small>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 19
                    echo "            ";
                }
                // line 20
                echo "            ";
                if ($this->getAttribute($context["product"], "recurring", array())) {
                    // line 21
                    echo "            <br />
            - <small>";
                    // line 22
                    echo (isset($context["text_recurring"]) ? $context["text_recurring"] : null);
                    echo " ";
                    echo $this->getAttribute($context["product"], "recurring", array());
                    echo "</small>
            ";
                }
                // line 24
                echo "          </td>
          <td class=\"remove\"><a onclick=\"cart.remove('";
                // line 25
                echo $this->getAttribute($context["product"], "cart_id", array());
                echo "');\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"remove\">&times;</a></td>
        </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["vouchers"]) ? $context["vouchers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
                // line 29
                echo "        <tr>
          <td colspan=\"2\" class=\"text-left\"><span class=\"product-name main-font\">";
                // line 30
                echo $this->getAttribute($context["voucher"], "description", array());
                echo "</span>
          1 x <span class=\"price\">";
                // line 31
                echo $this->getAttribute($context["voucher"], "amount", array());
                echo "</span></td>
          <td class=\"text-right\"><a onclick=\"voucher.remove('";
                // line 32
                echo $this->getAttribute($context["voucher"], "key", array());
                echo "');\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"remove\">&times;</a></td>
        </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 35
            echo "      </table>
    </li>
    <li>
      <div>
        <table class=\"table totals\">
          ";
            // line 40
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
                // line 41
                echo "          <tr>
            <td class=\"text-left\">";
                // line 42
                echo $this->getAttribute($context["total"], "title", array());
                echo "</td>
            <td class=\"text-right\">";
                // line 43
                echo $this->getAttribute($context["total"], "text", array());
                echo "</td>
          </tr>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "        </table>
        <a class=\"btn btn-default btn-block\" href=\"";
            // line 47
            echo (isset($context["cart"]) ? $context["cart"] : null);
            echo "\">";
            echo (isset($context["text_cart"]) ? $context["text_cart"] : null);
            echo "</a>
        <a class=\"btn btn-contrast btn-block\" href=\"";
            // line 48
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo "\">";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo "</a>
      </div>
    </li>
    ";
        } else {
            // line 52
            echo "    <li>
      <div class=\"table empty\">
      <div class=\"table-cell\"><i class=\"global-cart\"></i></div>
      <div class=\"table-cell\">";
            // line 55
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</div>
      </div>
    </li>
    ";
        }
        // line 59
        echo "  </ul>";
    }

    public function getTemplateName()
    {
        return "basel/template/common/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 59,  192 => 55,  187 => 52,  178 => 48,  172 => 47,  169 => 46,  160 => 43,  156 => 42,  153 => 41,  149 => 40,  142 => 35,  131 => 32,  127 => 31,  123 => 30,  120 => 29,  115 => 28,  104 => 25,  101 => 24,  94 => 22,  91 => 21,  88 => 20,  85 => 19,  75 => 17,  72 => 16,  67 => 15,  65 => 14,  59 => 13,  53 => 12,  50 => 11,  38 => 9,  36 => 8,  32 => 6,  28 => 5,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/*   <ul id="cart-content">*/
/*     {% if products or vouchers %}*/
/*     <li>*/
/*       <table class="table products">*/
/*         {% for product in products %}*/
/*         <tr>*/
/*           <td class="image">*/
/*           {% if product.thumb %}*/
/*             <a href="{{ product.href }}"><img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" /></a>*/
/*             {% endif %}*/
/*             </td>*/
/*           <td class="main"><a class="product-name main-font" href="{{ product.href }}">{{ product.name }}</a>*/
/*             {{ product.quantity }} x <span class="price">{{ product.price }}</span>*/
/*             {% if product.option %}*/
/*             {% for option in product.option %}*/
/*             <br />*/
/*             - <small>{{ option.name }} {{ option.value }}</small>*/
/*             {% endfor %}*/
/*             {% endif %}*/
/*             {% if product.recurring %}*/
/*             <br />*/
/*             - <small>{{ text_recurring }} {{ product.recurring }}</small>*/
/*             {% endif %}*/
/*           </td>*/
/*           <td class="remove"><a onclick="cart.remove('{{ product.cart_id }}');" title="{{ button_remove }}" class="remove">&times;</a></td>*/
/*         </tr>*/
/*         {% endfor %}*/
/*         {% for voucher in vouchers %}*/
/*         <tr>*/
/*           <td colspan="2" class="text-left"><span class="product-name main-font">{{ voucher.description }}</span>*/
/*           1 x <span class="price">{{ voucher.amount }}</span></td>*/
/*           <td class="text-right"><a onclick="voucher.remove('{{ voucher.key }}');" title="{{ button_remove }}" class="remove">&times;</a></td>*/
/*         </tr>*/
/*         {% endfor %}*/
/*       </table>*/
/*     </li>*/
/*     <li>*/
/*       <div>*/
/*         <table class="table totals">*/
/*           {% for total in totals %}*/
/*           <tr>*/
/*             <td class="text-left">{{ total.title }}</td>*/
/*             <td class="text-right">{{ total.text }}</td>*/
/*           </tr>*/
/*           {% endfor %}*/
/*         </table>*/
/*         <a class="btn btn-default btn-block" href="{{ cart }}">{{ text_cart }}</a>*/
/*         <a class="btn btn-contrast btn-block" href="{{ checkout }}">{{ text_checkout }}</a>*/
/*       </div>*/
/*     </li>*/
/*     {% else %}*/
/*     <li>*/
/*       <div class="table empty">*/
/*       <div class="table-cell"><i class="global-cart"></i></div>*/
/*       <div class="table-cell">{{ text_empty }}</div>*/
/*       </div>*/
/*     </li>*/
/*     {% endif %}*/
/*   </ul>*/
