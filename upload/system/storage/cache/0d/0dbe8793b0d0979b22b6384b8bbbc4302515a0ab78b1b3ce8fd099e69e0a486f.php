<?php

/* basel/template/common/position_bottom_half.twig */
class __TwigTemplate_0908af23936ca99ee9fa34063fa9a1f2790a3f2a4a2b2c56bf70821be08f285c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["modules"]) ? $context["modules"] : null)) {
            // line 2
            echo "<div class=\"row\">
";
            // line 3
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                // line 4
                echo "<div class=\"col-sm-6\">
";
                // line 5
                echo $context["module"];
                echo "
</div>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 8
            echo "</div>
";
        }
    }

    public function getTemplateName()
    {
        return "basel/template/common/position_bottom_half.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 8,  31 => 5,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if modules %}*/
/* <div class="row">*/
/* {% for module in modules %}*/
/* <div class="col-sm-6">*/
/* {{ module }}*/
/* </div>*/
/* {% endfor %}*/
/* </div>*/
/* {% endif %}*/
