<?php

/* extension/basel/panel_tabs/typography.twig */
class __TwigTemplate_84e94bb19808f44ca62a2de64d93a74544d501de44544e29faaaa784e34f9631 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Typography</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Use custom fonts instead of the default ones\">Override default font styles</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_typo_status]\" class=\"typo-select\" value=\"0\" ";
        // line 6
        if (((isset($context["basel_typo_status"]) ? $context["basel_typo_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_typo_status]\" class=\"typo-select\" value=\"1\" ";
        // line 7
        if (((isset($context["basel_typo_status"]) ? $context["basel_typo_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div id=\"basel_typo_holder\"";
        // line 11
        if ((isset($context["basel_typo_status"]) ? $context["basel_typo_status"] : null)) {
            echo " style=\"display:block\"";
        } else {
            echo " style=\"display:none\"";
        }
        echo ">
<legend class=\"sub\">Google Font List</legend>


<div class=\"col-sm-offset-2\" style=\"margin-bottom:10px;\">
<div class=\"bs-callout bs-callout-info bs-callout-sm\">
When you add new fonts to the list, please save the module before you can start using them in the font settings
</div>
</div>
<div class=\"row\">
<div class=\"col-sm-2\"></div>
<div class=\"col-sm-10\">
 <table id=\"fonts\" class=\"table table-clean\">
    <thead>
      <tr>
        <td width=\"49%\"><span data-toggle=\"tooltip\" title=\"For example: Roboto:400,600\">Font Import</span></td>
        <td width=\"49%\"><span data-toggle=\"tooltip\" title=\"For example: 'Roboto', sans-serif\">Font Name</span></td>
        <td width=\"2%\"></td>
      </tr>
    </thead>
    <tbody>
     ";
        // line 32
        $context["font_row"] = 1;
        // line 33
        echo "     ";
        if (array_key_exists("basel_fonts", $context)) {
            // line 34
            echo "      ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["basel_fonts"]) ? $context["basel_fonts"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["basel_font"]) {
                // line 35
                echo "      <tr id=\"font-row";
                echo (isset($context["font_row"]) ? $context["font_row"] : null);
                echo "\">
        <td class=\"first\">
        <div style=\"margin-bottom:5px;\">
        <input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_fonts][";
                // line 38
                echo (isset($context["font_row"]) ? $context["font_row"] : null);
                echo "][import]\" value=\"";
                echo (($this->getAttribute($context["basel_font"], "import", array())) ? ($this->getAttribute($context["basel_font"], "import", array())) : (""));
                echo "\" />
        </div>
        </td>
        <td class=\"first\">
        <input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_fonts][";
                // line 42
                echo (isset($context["font_row"]) ? $context["font_row"] : null);
                echo "][name]\" value=\"";
                echo (($this->getAttribute($context["basel_font"], "name", array())) ? ($this->getAttribute($context["basel_font"], "name", array())) : (""));
                echo "\" />
        </td>
        <td class=\"text-right\">
        <button type=\"button\" onclick=\"\$('#font-row";
                // line 45
                echo (isset($context["font_row"]) ? $context["font_row"] : null);
                echo "').remove();\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button>
        </td>
      </tr>
      ";
                // line 48
                $context["font_row"] = ((isset($context["font_row"]) ? $context["font_row"] : null) + 1);
                // line 49
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_font'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "   ";
        }
        // line 51
        echo "    </tbody>
    <tfoot>
      <tr>
        <td colspan=\"2\"></td>
        <td class=\"text-right\"><button type=\"button\" onclick=\"addFontRow();\" data-toggle=\"tooltip\" title=\"";
        // line 55
        echo (isset($context["button_add"]) ? $context["button_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus-circle\"></i></button></td>
      </tr>
    </tfoot>
  </table>
 </div>
 </div>
 
<legend>Font settings</legend>
<legend class=\"sub\">General</legend>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Body font</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][body_font_fam]\" class=\"form-control\">
    <option disabled style=\"font-weight:bold\">System Fonts</option>
    ";
        // line 70
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["system_fonts"]) ? $context["system_fonts"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["system_font"]) {
            // line 71
            echo "    <option value=\"";
            echo $context["key"];
            echo "\"";
            if (((isset($context["body_font_fam"]) ? $context["body_font_fam"] : null) == $context["key"])) {
                echo "selected=\"selected\"";
            }
            echo ">
    ";
            // line 72
            echo $context["system_font"];
            echo "</option>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['system_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "    <option disabled style=\"font-weight:bold\">Google Webfonts</option>
    ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["basel_fonts"]) ? $context["basel_fonts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["basel_font"]) {
            // line 76
            echo "    <option value=\"";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "\" ";
            if (((isset($context["body_font_fam"]) ? $context["body_font_fam"] : null) == $this->getAttribute($context["basel_font"], "name", array()))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "</option>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "    </select>
</div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Disable if your custom font dont support, or looks bad in italic\">Keep italics</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][body_font_italic_status]\" value=\"0\" ";
        // line 85
        if (((isset($context["body_font_italic_status"]) ? $context["body_font_italic_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][body_font_italic_status]\"  value=\"1\" ";
        // line 86
        if (((isset($context["body_font_italic_status"]) ? $context["body_font_italic_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Select default font weight for bold text\">Bold font weight</span></label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:160px\" name=\"settings[basel][body_font_bold_weight]\" value=\"";
        // line 93
        echo (((isset($context["body_font_bold_weight"]) ? $context["body_font_bold_weight"] : null)) ? ((isset($context["body_font_bold_weight"]) ? $context["body_font_bold_weight"] : null)) : ("600"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Contrast font is used on item names, in some widgets etc.\">Contrast font</span></label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][contrast_font_fam]\" class=\"form-control\">
    <option disabled style=\"font-weight:bold\">System Fonts</option>
    ";
        // line 102
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["system_fonts"]) ? $context["system_fonts"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["system_font"]) {
            // line 103
            echo "    <option value=\"";
            echo $context["key"];
            echo "\" ";
            if (((isset($context["contrast_font_fam"]) ? $context["contrast_font_fam"] : null) == $context["key"])) {
                echo "selected=\"selected\"";
            }
            echo ">
    ";
            // line 104
            echo $context["system_font"];
            echo "</option>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['system_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "    <option disabled style=\"font-weight:bold\">Google Webfonts</option>
    ";
        // line 107
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["basel_fonts"]) ? $context["basel_fonts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["basel_font"]) {
            // line 108
            echo "    <option value=\"";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "\" ";
            if (((isset($context["contrast_font_fam"]) ? $context["contrast_font_fam"] : null) == $this->getAttribute($context["basel_font"], "name", array()))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "</option>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 110
        echo "    </select>
</div>                   
</div>

<legend class=\"sub\">Font size adjustment</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Use this to adjust if your font looks bigger or smaller than desirably\">Larger font size (16px)</span></label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:160px\" name=\"settings[basel][body_font_size_16]\" value=\"";
        // line 118
        echo (((isset($context["body_font_size_16"]) ? $context["body_font_size_16"] : null)) ? ((isset($context["body_font_size_16"]) ? $context["body_font_size_16"] : null)) : ("16px"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Use this to adjust if your font looks bigger or smaller than desirably\">Large font size (15px)</span></label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:160px\" name=\"settings[basel][body_font_size_15]\" value=\"";
        // line 125
        echo (((isset($context["body_font_size_15"]) ? $context["body_font_size_15"] : null)) ? ((isset($context["body_font_size_15"]) ? $context["body_font_size_15"] : null)) : ("15px"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Use this to adjust if your font looks bigger or smaller than desirably\">Normal font size (14px)</span></label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:160px\" name=\"settings[basel][body_font_size_14]\" value=\"";
        // line 132
        echo (((isset($context["body_font_size_14"]) ? $context["body_font_size_14"] : null)) ? ((isset($context["body_font_size_14"]) ? $context["body_font_size_14"] : null)) : ("14px"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Use this to adjust if your font looks bigger or smaller than desirably\">Small font size (13px)</span></label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:160px\" name=\"settings[basel][body_font_size_13]\" value=\"";
        // line 139
        echo (((isset($context["body_font_size_13"]) ? $context["body_font_size_13"] : null)) ? ((isset($context["body_font_size_13"]) ? $context["body_font_size_13"] : null)) : ("13px"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Use this to adjust if your font looks bigger or smaller than desirably\">Tiny font size (12px)</span></label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:160px\" name=\"settings[basel][body_font_size_12]\" value=\"";
        // line 146
        echo (((isset($context["body_font_size_12"]) ? $context["body_font_size_12"] : null)) ? ((isset($context["body_font_size_12"]) ? $context["body_font_size_12"] : null)) : ("12px"));
        echo "\" />
    </div>                   
</div>

<legend class=\"sub\">Headings</legend>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Headings font</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][headings_fam]\" class=\"form-control\">
    <option disabled style=\"font-weight:bold\">System Fonts</option>
    ";
        // line 157
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["system_fonts"]) ? $context["system_fonts"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["system_font"]) {
            // line 158
            echo "    <option value=\"";
            echo $context["key"];
            echo "\" 
    ";
            // line 159
            if (((isset($context["headings_fam"]) ? $context["headings_fam"] : null) == $context["key"])) {
                echo "selected=\"selected\"";
            }
            echo ">
    ";
            // line 160
            echo $context["system_font"];
            echo "</option>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['system_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 162
        echo "    <option disabled style=\"font-weight:bold\">Google Webfonts</option>
    ";
        // line 163
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["basel_fonts"]) ? $context["basel_fonts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["basel_font"]) {
            // line 164
            echo "    <option value=\"";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "\" ";
            if (((isset($context["headings_fam"]) ? $context["headings_fam"] : null) == $this->getAttribute($context["basel_font"], "name", array()))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "</option>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 166
        echo "    </select>
</div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Headings font weight</label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:160px\" name=\"settings[basel][headings_weight]\" value=\"";
        // line 173
        echo (((isset($context["headings_weight"]) ? $context["headings_weight"] : null)) ? ((isset($context["headings_weight"]) ? $context["headings_weight"] : null)) : ("600"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Smaller headings size</label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:160px\" name=\"settings[basel][headings_size_sm]\" value=\"";
        // line 180
        echo (((isset($context["headings_size_sm"]) ? $context["headings_size_sm"] : null)) ? ((isset($context["headings_size_sm"]) ? $context["headings_size_sm"] : null)) : ("20px"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Largeer headings size</label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:160px\" name=\"settings[basel][headings_size_lg]\" value=\"";
        // line 187
        echo (((isset($context["headings_size_lg"]) ? $context["headings_size_lg"] : null)) ? ((isset($context["headings_size_lg"]) ? $context["headings_size_lg"] : null)) : ("28px"));
        echo "\" />
    </div>                   
</div>

<legend class=\"sub\">Page titles / H1</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><br /><span data-toggle=\"tooltip\" title=\"Inline titles are page titles that are not moved to the breadcrumb\">Inline titles</span></label>
    <div class=\"col-sm-10\">
    <div class=\"row\">
        <div class=\"col-sm-4\">
        <label>Font family</label>
        <select name=\"settings[basel][h1_inline_fam]\" class=\"form-control\">
        <option disabled style=\"font-weight:bold\">System Fonts</option>
        ";
        // line 200
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["system_fonts"]) ? $context["system_fonts"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["system_font"]) {
            // line 201
            echo "        <option value=\"";
            echo $context["key"];
            echo "\" 
        ";
            // line 202
            if (((isset($context["h1_inline_fam"]) ? $context["h1_inline_fam"] : null) == $context["key"])) {
                echo "selected=\"selected\"";
            }
            echo ">
        ";
            // line 203
            echo $context["system_font"];
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['system_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 205
        echo "        <option disabled style=\"font-weight:bold\">Google Webfonts</option>
        ";
        // line 206
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["basel_fonts"]) ? $context["basel_fonts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["basel_font"]) {
            // line 207
            echo "        <option value=\"";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "\" ";
            if (((isset($context["h1_inline_fam"]) ? $context["h1_inline_fam"] : null) == $this->getAttribute($context["basel_font"], "name", array()))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 209
        echo "        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Font size</label>
        <input class=\"form-control\" name=\"settings[basel][h1_inline_size]\" value=\"";
        // line 213
        echo (((isset($context["h1_inline_size"]) ? $context["h1_inline_size"] : null)) ? ((isset($context["h1_inline_size"]) ? $context["h1_inline_size"] : null)) : ("34px"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Font weight</label>
        <input class=\"form-control\" name=\"settings[basel][h1_inline_weight]\" value=\"";
        // line 217
        echo (((isset($context["h1_inline_weight"]) ? $context["h1_inline_weight"] : null)) ? ((isset($context["h1_inline_weight"]) ? $context["h1_inline_weight"] : null)) : ("600"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Text transform</label>
        <select name=\"settings[basel][h1_inline_trans]\" class=\"form-control\">
        <option value=\"none\"";
        // line 222
        if (((isset($context["h1_inline_trans"]) ? $context["h1_inline_trans"] : null) == "none")) {
            echo " selected=\"selected\"";
        }
        echo ">None</option>
        <option value=\"uppercase\"";
        // line 223
        if (((isset($context["h1_inline_trans"]) ? $context["h1_inline_trans"] : null) == "uppercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Uppercase</option>
        <option value=\"lowercase\"";
        // line 224
        if (((isset($context["h1_inline_trans"]) ? $context["h1_inline_trans"] : null) == "lowercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Lowercase</option>
        <option value=\"capitalize\"";
        // line 225
        if (((isset($context["h1_inline_trans"]) ? $context["h1_inline_trans"] : null) == "capitalize")) {
            echo " selected=\"selected\"";
        }
        echo ">Capitalize</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Letter spcaing</label>
        <input class=\"form-control\" name=\"settings[basel][h1_inline_ls]\" value=\"";
        // line 230
        echo (((isset($context["h1_inline_ls"]) ? $context["h1_inline_ls"] : null)) ? ((isset($context["h1_inline_ls"]) ? $context["h1_inline_ls"] : null)) : ("0px"));
        echo "\" />
        </div>
    </div>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><br />Titles inside breadcrumb</label>
    <div class=\"col-sm-10\">
    <div class=\"row\">
        <div class=\"col-sm-4\">
        <label>Font family</label>
        <select name=\"settings[basel][h1_breadcrumb_fam]\" class=\"form-control\">
        <option disabled style=\"font-weight:bold\">System Fonts</option>
        ";
        // line 244
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["system_fonts"]) ? $context["system_fonts"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["system_font"]) {
            // line 245
            echo "        <option value=\"";
            echo $context["key"];
            echo "\" 
        ";
            // line 246
            if (((isset($context["h1_breadcrumb_fam"]) ? $context["h1_breadcrumb_fam"] : null) == $context["key"])) {
                echo "selected=\"selected\"";
            }
            echo ">
        ";
            // line 247
            echo $context["system_font"];
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['system_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 249
        echo "        <option disabled style=\"font-weight:bold\">Google Webfonts</option>
        ";
        // line 250
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["basel_fonts"]) ? $context["basel_fonts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["basel_font"]) {
            // line 251
            echo "        <option value=\"";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "\" ";
            if (((isset($context["h1_breadcrumb_fam"]) ? $context["h1_breadcrumb_fam"] : null) == $this->getAttribute($context["basel_font"], "name", array()))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 253
        echo "        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Font size</label>
        <input class=\"form-control\" name=\"settings[basel][h1_breadcrumb_size]\" value=\"";
        // line 257
        echo (((isset($context["h1_breadcrumb_size"]) ? $context["h1_breadcrumb_size"] : null)) ? ((isset($context["h1_breadcrumb_size"]) ? $context["h1_breadcrumb_size"] : null)) : ("34px"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Font weight</label>
        <input class=\"form-control\" name=\"settings[basel][h1_breadcrumb_weight]\" value=\"";
        // line 261
        echo (((isset($context["h1_breadcrumb_weight"]) ? $context["h1_breadcrumb_weight"] : null)) ? ((isset($context["h1_breadcrumb_weight"]) ? $context["h1_breadcrumb_weight"] : null)) : ("600"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Text transform</label>
        <select name=\"settings[basel][h1_breadcrumb_trans]\" class=\"form-control\">
        <option value=\"none\"";
        // line 266
        if (((isset($context["h1_breadcrumb_trans"]) ? $context["h1_breadcrumb_trans"] : null) == "none")) {
            echo " selected=\"selected\"";
        }
        echo ">None</option>
        <option value=\"uppercase\"";
        // line 267
        if (((isset($context["h1_breadcrumb_trans"]) ? $context["h1_breadcrumb_trans"] : null) == "uppercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Uppercase</option>
        <option value=\"lowercase\"";
        // line 268
        if (((isset($context["h1_breadcrumb_trans"]) ? $context["h1_breadcrumb_trans"] : null) == "lowercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Lowercase</option>
        <option value=\"capitalize\"";
        // line 269
        if (((isset($context["h1_breadcrumb_trans"]) ? $context["h1_breadcrumb_trans"] : null) == "capitalize")) {
            echo " selected=\"selected\"";
        }
        echo ">Capitalize</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Letter spcaing</label>
        <input class=\"form-control\" name=\"settings[basel][h1_breadcrumb_ls]\" value=\"";
        // line 274
        echo (((isset($context["h1_breadcrumb_ls"]) ? $context["h1_breadcrumb_ls"] : null)) ? ((isset($context["h1_breadcrumb_ls"]) ? $context["h1_breadcrumb_ls"] : null)) : ("0px"));
        echo "\" />
        </div>
    </div>
    </div>                   
</div>

<legend class=\"sub\">Widgets Titles / Module Titles</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><br /><span data-toggle=\"tooltip\" title=\"Widgets not positioned in column areas\">Widgets in content</span></label>
    <div class=\"col-sm-10\">
    <div class=\"row\">
        <div class=\"col-sm-4\">
        <label>Font family</label>
        <select name=\"settings[basel][widget_lg_fam]\" class=\"form-control\">
        <option disabled style=\"font-weight:bold\">System Fonts</option>
        ";
        // line 289
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["system_fonts"]) ? $context["system_fonts"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["system_font"]) {
            // line 290
            echo "        <option value=\"";
            echo $context["key"];
            echo "\" 
        ";
            // line 291
            if (((isset($context["widget_lg_fam"]) ? $context["widget_lg_fam"] : null) == $context["key"])) {
                echo "selected=\"selected\"";
            }
            echo ">
        ";
            // line 292
            echo $context["system_font"];
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['system_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 294
        echo "        <option disabled style=\"font-weight:bold\">Google Webfonts</option>
        ";
        // line 295
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["basel_fonts"]) ? $context["basel_fonts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["basel_font"]) {
            // line 296
            echo "        <option value=\"";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "\" ";
            if (((isset($context["widget_lg_fam"]) ? $context["widget_lg_fam"] : null) == $this->getAttribute($context["basel_font"], "name", array()))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 298
        echo "        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Font size</label>
        <input class=\"form-control\" name=\"settings[basel][widget_lg_size]\" value=\"";
        // line 302
        echo (((isset($context["widget_lg_size"]) ? $context["widget_lg_size"] : null)) ? ((isset($context["widget_lg_size"]) ? $context["widget_lg_size"] : null)) : ("26px"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Font weight</label>
        <input class=\"form-control\" name=\"settings[basel][widget_lg_weight]\" value=\"";
        // line 306
        echo (((isset($context["widget_lg_weight"]) ? $context["widget_lg_weight"] : null)) ? ((isset($context["widget_lg_weight"]) ? $context["widget_lg_weight"] : null)) : ("600"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Text transform</label>
        <select name=\"settings[basel][widget_lg_trans]\" class=\"form-control\">
        <option value=\"none\"";
        // line 311
        if (((isset($context["widget_lg_trans"]) ? $context["widget_lg_trans"] : null) == "none")) {
            echo " selected=\"selected\"";
        }
        echo ">None</option>
        <option value=\"uppercase\"";
        // line 312
        if (((isset($context["widget_lg_trans"]) ? $context["widget_lg_trans"] : null) == "uppercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Uppercase</option>
        <option value=\"lowercase\"";
        // line 313
        if (((isset($context["widget_lg_trans"]) ? $context["widget_lg_trans"] : null) == "lowercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Lowercase</option>
        <option value=\"capitalize\"";
        // line 314
        if (((isset($context["widget_lg_trans"]) ? $context["widget_lg_trans"] : null) == "capitalize")) {
            echo " selected=\"selected\"";
        }
        echo ">Capitalize</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Letter spcaing</label>
        <input class=\"form-control\" name=\"settings[basel][widget_lg_ls]\" value=\"";
        // line 319
        echo (((isset($context["widget_lg_ls"]) ? $context["widget_lg_ls"] : null)) ? ((isset($context["widget_lg_ls"]) ? $context["widget_lg_ls"] : null)) : ("0px"));
        echo "\" />
        </div>
    </div>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><br />Widgets in column</label>
    <div class=\"col-sm-10\">
    <div class=\"row\">
        <div class=\"col-sm-4\">
        <label>Font family</label>
        <select name=\"settings[basel][widget_sm_fam]\" class=\"form-control\">
        <option disabled style=\"font-weight:bold\">System Fonts</option>
        ";
        // line 333
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["system_fonts"]) ? $context["system_fonts"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["system_font"]) {
            // line 334
            echo "        <option value=\"";
            echo $context["key"];
            echo "\" 
        ";
            // line 335
            if (((isset($context["widget_sm_fam"]) ? $context["widget_sm_fam"] : null) == $context["key"])) {
                echo "selected=\"selected\"";
            }
            echo ">
        ";
            // line 336
            echo $context["system_font"];
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['system_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 338
        echo "        <option disabled style=\"font-weight:bold\">Google Webfonts</option>
        ";
        // line 339
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["basel_fonts"]) ? $context["basel_fonts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["basel_font"]) {
            // line 340
            echo "        <option value=\"";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "\" ";
            if (((isset($context["widget_sm_fam"]) ? $context["widget_sm_fam"] : null) == $this->getAttribute($context["basel_font"], "name", array()))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 342
        echo "        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Font size</label>
        <input class=\"form-control\" name=\"settings[basel][widget_sm_size]\" value=\"";
        // line 346
        echo (((isset($context["widget_sm_size"]) ? $context["widget_sm_size"] : null)) ? ((isset($context["widget_sm_size"]) ? $context["widget_sm_size"] : null)) : ("16px"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Font weight</label>
        <input class=\"form-control\" name=\"settings[basel][widget_sm_weight]\" value=\"";
        // line 350
        echo (((isset($context["widget_sm_weight"]) ? $context["widget_sm_weight"] : null)) ? ((isset($context["widget_sm_weight"]) ? $context["widget_sm_weight"] : null)) : ("600"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Text transform</label>
        <select name=\"settings[basel][widget_sm_trans]\" class=\"form-control\">
        <option value=\"none\"";
        // line 355
        if (((isset($context["widget_sm_trans"]) ? $context["widget_sm_trans"] : null) == "none")) {
            echo " selected=\"selected\"";
        }
        echo ">None</option>
        <option value=\"uppercase\"";
        // line 356
        if (((isset($context["widget_sm_trans"]) ? $context["widget_sm_trans"] : null) == "uppercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Uppercase</option>
        <option value=\"lowercase\"";
        // line 357
        if (((isset($context["widget_sm_trans"]) ? $context["widget_sm_trans"] : null) == "lowercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Lowercase</option>
        <option value=\"capitalize\"";
        // line 358
        if (((isset($context["widget_sm_trans"]) ? $context["widget_sm_trans"] : null) == "capitalize")) {
            echo " selected=\"selected\"";
        }
        echo ">Capitalize</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Letter spcaing</label>
        <input class=\"form-control\" name=\"settings[basel][widget_sm_ls]\" value=\"";
        // line 363
        echo (((isset($context["widget_sm_ls"]) ? $context["widget_sm_ls"] : null)) ? ((isset($context["widget_sm_ls"]) ? $context["widget_sm_ls"] : null)) : ("0.75px"));
        echo "\" />
        </div>
    </div>
    </div>                   
</div>

<legend class=\"sub\">Main Menu In Header</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><br />Main menu</label>
    <div class=\"col-sm-10\">
    <div class=\"row\">
        <div class=\"col-sm-4\">
        <label>Font family</label>
        <select name=\"settings[basel][menu_font_fam]\" class=\"form-control\">
        <option disabled style=\"font-weight:bold\">System Fonts</option>
        ";
        // line 378
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["system_fonts"]) ? $context["system_fonts"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["system_font"]) {
            // line 379
            echo "        <option value=\"";
            echo $context["key"];
            echo "\" 
        ";
            // line 380
            if (((isset($context["menu_font_fam"]) ? $context["menu_font_fam"] : null) == $context["key"])) {
                echo "selected=\"selected\"";
            }
            echo ">
        ";
            // line 381
            echo $context["system_font"];
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['system_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 383
        echo "        <option disabled style=\"font-weight:bold\">Google Webfonts</option>
        ";
        // line 384
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["basel_fonts"]) ? $context["basel_fonts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["basel_font"]) {
            // line 385
            echo "        <option value=\"";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "\" ";
            if (((isset($context["menu_font_fam"]) ? $context["menu_font_fam"] : null) == $this->getAttribute($context["basel_font"], "name", array()))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $this->getAttribute($context["basel_font"], "name", array());
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_font'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 387
        echo "        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Font size</label>
        <input class=\"form-control\" name=\"settings[basel][menu_font_size]\" value=\"";
        // line 391
        echo (((isset($context["menu_font_size"]) ? $context["menu_font_size"] : null)) ? ((isset($context["menu_font_size"]) ? $context["menu_font_size"] : null)) : ("14px"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Font weight</label>
        <input class=\"form-control\" name=\"settings[basel][menu_font_weight]\" value=\"";
        // line 395
        echo (((isset($context["menu_font_weight"]) ? $context["menu_font_weight"] : null)) ? ((isset($context["menu_font_weight"]) ? $context["menu_font_weight"] : null)) : ("400"));
        echo "\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Text transform</label>
        <select name=\"settings[basel][menu_font_trans]\" class=\"form-control\">
        <option value=\"none\"";
        // line 400
        if (((isset($context["menu_font_trans"]) ? $context["menu_font_trans"] : null) == "none")) {
            echo " selected=\"selected\"";
        }
        echo ">None</option>
        <option value=\"uppercase\"";
        // line 401
        if (((isset($context["menu_font_trans"]) ? $context["menu_font_trans"] : null) == "uppercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Uppercase</option>
        <option value=\"lowercase\"";
        // line 402
        if (((isset($context["menu_font_trans"]) ? $context["menu_font_trans"] : null) == "lowercase")) {
            echo " selected=\"selected\"";
        }
        echo ">Lowercase</option>
        <option value=\"capitalize\"";
        // line 403
        if (((isset($context["menu_font_trans"]) ? $context["menu_font_trans"] : null) == "capitalize")) {
            echo " selected=\"selected\"";
        }
        echo ">Capitalize</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Letter spcaing</label>
        <input class=\"form-control\" name=\"settings[basel][menu_font_ls]\" value=\"";
        // line 408
        echo (((isset($context["menu_font_ls"]) ? $context["menu_font_ls"] : null)) ? ((isset($context["menu_font_ls"]) ? $context["menu_font_ls"] : null)) : ("0.5px"));
        echo "\" />
        </div>
    </div>
    </div>                   
</div> 

</div><!-- #basel_typo_holder ends -->


<script type=\"text/javascript\"><!--
var font_row = ";
        // line 418
        echo (isset($context["font_row"]) ? $context["font_row"] : null);
        echo ";
function addFontRow() {
\thtml  = '<tr id=\"font-row' + font_row + '\">';
\thtml += '<td class=\"first\">';
\thtml += '<input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_fonts][' + font_row + '][import]\" />';
\thtml += '</td>';
\thtml += '<td class=\"first\">';
\thtml += '<input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_fonts][' + font_row + '][name]\" />';
\thtml += '</td>';
\thtml += '<td class=\"text-right\"><button type=\"button\" onclick=\"\$(\\'#font-row' + font_row  + '\\').remove();\" data-toggle=\"tooltip\" title=\"";
        // line 427
        echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
        echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>';
\thtml += '</tr>';
\t\$('#fonts tbody').append(html);
\tfont_row++;
}
//--></script>";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/typography.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  990 => 427,  978 => 418,  965 => 408,  955 => 403,  949 => 402,  943 => 401,  937 => 400,  929 => 395,  922 => 391,  916 => 387,  901 => 385,  897 => 384,  894 => 383,  886 => 381,  880 => 380,  875 => 379,  871 => 378,  853 => 363,  843 => 358,  837 => 357,  831 => 356,  825 => 355,  817 => 350,  810 => 346,  804 => 342,  789 => 340,  785 => 339,  782 => 338,  774 => 336,  768 => 335,  763 => 334,  759 => 333,  742 => 319,  732 => 314,  726 => 313,  720 => 312,  714 => 311,  706 => 306,  699 => 302,  693 => 298,  678 => 296,  674 => 295,  671 => 294,  663 => 292,  657 => 291,  652 => 290,  648 => 289,  630 => 274,  620 => 269,  614 => 268,  608 => 267,  602 => 266,  594 => 261,  587 => 257,  581 => 253,  566 => 251,  562 => 250,  559 => 249,  551 => 247,  545 => 246,  540 => 245,  536 => 244,  519 => 230,  509 => 225,  503 => 224,  497 => 223,  491 => 222,  483 => 217,  476 => 213,  470 => 209,  455 => 207,  451 => 206,  448 => 205,  440 => 203,  434 => 202,  429 => 201,  425 => 200,  409 => 187,  399 => 180,  389 => 173,  380 => 166,  365 => 164,  361 => 163,  358 => 162,  350 => 160,  344 => 159,  339 => 158,  335 => 157,  321 => 146,  311 => 139,  301 => 132,  291 => 125,  281 => 118,  271 => 110,  256 => 108,  252 => 107,  249 => 106,  241 => 104,  232 => 103,  228 => 102,  216 => 93,  204 => 86,  198 => 85,  189 => 78,  174 => 76,  170 => 75,  167 => 74,  159 => 72,  150 => 71,  146 => 70,  128 => 55,  122 => 51,  119 => 50,  113 => 49,  111 => 48,  103 => 45,  95 => 42,  86 => 38,  79 => 35,  74 => 34,  71 => 33,  69 => 32,  41 => 11,  32 => 7,  26 => 6,  19 => 1,);
    }
}
/* <legend>Typography</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Use custom fonts instead of the default ones">Override default font styles</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_typo_status]" class="typo-select" value="0" {% if basel_typo_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_typo_status]" class="typo-select" value="1" {% if basel_typo_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div id="basel_typo_holder"{% if basel_typo_status %} style="display:block"{% else %} style="display:none"{% endif %}>*/
/* <legend class="sub">Google Font List</legend>*/
/* */
/* */
/* <div class="col-sm-offset-2" style="margin-bottom:10px;">*/
/* <div class="bs-callout bs-callout-info bs-callout-sm">*/
/* When you add new fonts to the list, please save the module before you can start using them in the font settings*/
/* </div>*/
/* </div>*/
/* <div class="row">*/
/* <div class="col-sm-2"></div>*/
/* <div class="col-sm-10">*/
/*  <table id="fonts" class="table table-clean">*/
/*     <thead>*/
/*       <tr>*/
/*         <td width="49%"><span data-toggle="tooltip" title="For example: Roboto:400,600">Font Import</span></td>*/
/*         <td width="49%"><span data-toggle="tooltip" title="For example: 'Roboto', sans-serif">Font Name</span></td>*/
/*         <td width="2%"></td>*/
/*       </tr>*/
/*     </thead>*/
/*     <tbody>*/
/*      {% set font_row = 1 %}*/
/*      {% if basel_fonts is defined %}*/
/*       {% for basel_font in basel_fonts %}*/
/*       <tr id="font-row{{ font_row }}">*/
/*         <td class="first">*/
/*         <div style="margin-bottom:5px;">*/
/*         <input type="text" class="form-control" name="settings[basel][basel_fonts][{{ font_row }}][import]" value="{{ basel_font.import ? basel_font.import }}" />*/
/*         </div>*/
/*         </td>*/
/*         <td class="first">*/
/*         <input type="text" class="form-control" name="settings[basel][basel_fonts][{{ font_row }}][name]" value="{{ basel_font.name ? basel_font.name }}" />*/
/*         </td>*/
/*         <td class="text-right">*/
/*         <button type="button" onclick="$('#font-row{{ font_row }}').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>*/
/*         </td>*/
/*       </tr>*/
/*       {% set font_row = font_row + 1 %}*/
/*     {% endfor %}*/
/*    {% endif %}*/
/*     </tbody>*/
/*     <tfoot>*/
/*       <tr>*/
/*         <td colspan="2"></td>*/
/*         <td class="text-right"><button type="button" onclick="addFontRow();" data-toggle="tooltip" title="{{ button_add }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>*/
/*       </tr>*/
/*     </tfoot>*/
/*   </table>*/
/*  </div>*/
/*  </div>*/
/*  */
/* <legend>Font settings</legend>*/
/* <legend class="sub">General</legend>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Body font</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][body_font_fam]" class="form-control">*/
/*     <option disabled style="font-weight:bold">System Fonts</option>*/
/*     {% for key, system_font in system_fonts %}*/
/*     <option value="{{ key }}"{% if body_font_fam == key %}selected="selected"{% endif %}>*/
/*     {{ system_font }}</option>*/
/*     {% endfor %}*/
/*     <option disabled style="font-weight:bold">Google Webfonts</option>*/
/*     {% for basel_font in basel_fonts %}*/
/*     <option value="{{ basel_font.name }}" {% if body_font_fam == basel_font.name %}selected="selected"{% endif %}>{{ basel_font.name }}</option>*/
/*     {% endfor %}*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Disable if your custom font dont support, or looks bad in italic">Keep italics</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][body_font_italic_status]" value="0" {% if body_font_italic_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][body_font_italic_status]"  value="1" {% if body_font_italic_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Select default font weight for bold text">Bold font weight</span></label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:160px" name="settings[basel][body_font_bold_weight]" value="{{ body_font_bold_weight ? body_font_bold_weight : '600' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Contrast font is used on item names, in some widgets etc.">Contrast font</span></label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][contrast_font_fam]" class="form-control">*/
/*     <option disabled style="font-weight:bold">System Fonts</option>*/
/*     {% for key, system_font in system_fonts %}*/
/*     <option value="{{ key }}" {% if contrast_font_fam == key %}selected="selected"{% endif %}>*/
/*     {{ system_font }}</option>*/
/*     {% endfor %}*/
/*     <option disabled style="font-weight:bold">Google Webfonts</option>*/
/*     {% for basel_font in basel_fonts %}*/
/*     <option value="{{ basel_font.name }}" {% if contrast_font_fam == basel_font.name %}selected="selected"{% endif %}>{{ basel_font.name }}</option>*/
/*     {% endfor %}*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Font size adjustment</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Use this to adjust if your font looks bigger or smaller than desirably">Larger font size (16px)</span></label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:160px" name="settings[basel][body_font_size_16]" value="{{ body_font_size_16 ? body_font_size_16 : '16px' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Use this to adjust if your font looks bigger or smaller than desirably">Large font size (15px)</span></label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:160px" name="settings[basel][body_font_size_15]" value="{{ body_font_size_15 ? body_font_size_15 : '15px' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Use this to adjust if your font looks bigger or smaller than desirably">Normal font size (14px)</span></label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:160px" name="settings[basel][body_font_size_14]" value="{{ body_font_size_14 ? body_font_size_14 : '14px' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Use this to adjust if your font looks bigger or smaller than desirably">Small font size (13px)</span></label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:160px" name="settings[basel][body_font_size_13]" value="{{ body_font_size_13 ? body_font_size_13 : '13px' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Use this to adjust if your font looks bigger or smaller than desirably">Tiny font size (12px)</span></label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:160px" name="settings[basel][body_font_size_12]" value="{{ body_font_size_12 ? body_font_size_12 : '12px' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Headings</legend>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Headings font</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][headings_fam]" class="form-control">*/
/*     <option disabled style="font-weight:bold">System Fonts</option>*/
/*     {% for key, system_font in system_fonts %}*/
/*     <option value="{{ key }}" */
/*     {% if headings_fam == key %}selected="selected"{% endif %}>*/
/*     {{ system_font }}</option>*/
/*     {% endfor %}*/
/*     <option disabled style="font-weight:bold">Google Webfonts</option>*/
/*     {% for basel_font in basel_fonts %}*/
/*     <option value="{{ basel_font.name }}" {% if headings_fam == basel_font.name %}selected="selected"{% endif %}>{{ basel_font.name }}</option>*/
/*     {% endfor %}*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Headings font weight</label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:160px" name="settings[basel][headings_weight]" value="{{ headings_weight ? headings_weight : '600' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Smaller headings size</label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:160px" name="settings[basel][headings_size_sm]" value="{{ headings_size_sm ? headings_size_sm : '20px' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Largeer headings size</label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:160px" name="settings[basel][headings_size_lg]" value="{{ headings_size_lg ? headings_size_lg : '28px' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Page titles / H1</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><br /><span data-toggle="tooltip" title="Inline titles are page titles that are not moved to the breadcrumb">Inline titles</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="row">*/
/*         <div class="col-sm-4">*/
/*         <label>Font family</label>*/
/*         <select name="settings[basel][h1_inline_fam]" class="form-control">*/
/*         <option disabled style="font-weight:bold">System Fonts</option>*/
/*         {% for key, system_font in system_fonts %}*/
/*         <option value="{{ key }}" */
/*         {% if h1_inline_fam == key %}selected="selected"{% endif %}>*/
/*         {{ system_font }}</option>*/
/*         {% endfor %}*/
/*         <option disabled style="font-weight:bold">Google Webfonts</option>*/
/*         {% for basel_font in basel_fonts %}*/
/*         <option value="{{ basel_font.name }}" {% if h1_inline_fam == basel_font.name %}selected="selected"{% endif %}>{{ basel_font.name }}</option>*/
/*         {% endfor %}*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font size</label>*/
/*         <input class="form-control" name="settings[basel][h1_inline_size]" value="{{ h1_inline_size ? h1_inline_size : '34px' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font weight</label>*/
/*         <input class="form-control" name="settings[basel][h1_inline_weight]" value="{{ h1_inline_weight ? h1_inline_weight : '600' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Text transform</label>*/
/*         <select name="settings[basel][h1_inline_trans]" class="form-control">*/
/*         <option value="none"{% if h1_inline_trans == 'none' %} selected="selected"{% endif %}>None</option>*/
/*         <option value="uppercase"{% if h1_inline_trans == 'uppercase' %} selected="selected"{% endif %}>Uppercase</option>*/
/*         <option value="lowercase"{% if h1_inline_trans == 'lowercase' %} selected="selected"{% endif %}>Lowercase</option>*/
/*         <option value="capitalize"{% if h1_inline_trans == 'capitalize' %} selected="selected"{% endif %}>Capitalize</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Letter spcaing</label>*/
/*         <input class="form-control" name="settings[basel][h1_inline_ls]" value="{{ h1_inline_ls ? h1_inline_ls : '0px' }}" />*/
/*         </div>*/
/*     </div>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><br />Titles inside breadcrumb</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="row">*/
/*         <div class="col-sm-4">*/
/*         <label>Font family</label>*/
/*         <select name="settings[basel][h1_breadcrumb_fam]" class="form-control">*/
/*         <option disabled style="font-weight:bold">System Fonts</option>*/
/*         {% for key, system_font in system_fonts %}*/
/*         <option value="{{ key }}" */
/*         {% if h1_breadcrumb_fam == key %}selected="selected"{% endif %}>*/
/*         {{ system_font }}</option>*/
/*         {% endfor %}*/
/*         <option disabled style="font-weight:bold">Google Webfonts</option>*/
/*         {% for basel_font in basel_fonts %}*/
/*         <option value="{{ basel_font.name }}" {% if h1_breadcrumb_fam == basel_font.name %}selected="selected"{% endif %}>{{ basel_font.name }}</option>*/
/*         {% endfor %}*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font size</label>*/
/*         <input class="form-control" name="settings[basel][h1_breadcrumb_size]" value="{{ h1_breadcrumb_size ? h1_breadcrumb_size : '34px' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font weight</label>*/
/*         <input class="form-control" name="settings[basel][h1_breadcrumb_weight]" value="{{ h1_breadcrumb_weight ? h1_breadcrumb_weight : '600' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Text transform</label>*/
/*         <select name="settings[basel][h1_breadcrumb_trans]" class="form-control">*/
/*         <option value="none"{% if h1_breadcrumb_trans == 'none' %} selected="selected"{% endif %}>None</option>*/
/*         <option value="uppercase"{% if h1_breadcrumb_trans == 'uppercase' %} selected="selected"{% endif %}>Uppercase</option>*/
/*         <option value="lowercase"{% if h1_breadcrumb_trans == 'lowercase' %} selected="selected"{% endif %}>Lowercase</option>*/
/*         <option value="capitalize"{% if h1_breadcrumb_trans == 'capitalize' %} selected="selected"{% endif %}>Capitalize</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Letter spcaing</label>*/
/*         <input class="form-control" name="settings[basel][h1_breadcrumb_ls]" value="{{ h1_breadcrumb_ls ? h1_breadcrumb_ls : '0px' }}" />*/
/*         </div>*/
/*     </div>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Widgets Titles / Module Titles</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><br /><span data-toggle="tooltip" title="Widgets not positioned in column areas">Widgets in content</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="row">*/
/*         <div class="col-sm-4">*/
/*         <label>Font family</label>*/
/*         <select name="settings[basel][widget_lg_fam]" class="form-control">*/
/*         <option disabled style="font-weight:bold">System Fonts</option>*/
/*         {% for key, system_font in system_fonts %}*/
/*         <option value="{{ key }}" */
/*         {% if widget_lg_fam == key %}selected="selected"{% endif %}>*/
/*         {{ system_font }}</option>*/
/*         {% endfor %}*/
/*         <option disabled style="font-weight:bold">Google Webfonts</option>*/
/*         {% for basel_font in basel_fonts %}*/
/*         <option value="{{ basel_font.name }}" {% if widget_lg_fam == basel_font.name %}selected="selected"{% endif %}>{{ basel_font.name }}</option>*/
/*         {% endfor %}*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font size</label>*/
/*         <input class="form-control" name="settings[basel][widget_lg_size]" value="{{ widget_lg_size ? widget_lg_size : '26px' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font weight</label>*/
/*         <input class="form-control" name="settings[basel][widget_lg_weight]" value="{{ widget_lg_weight ? widget_lg_weight : '600' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Text transform</label>*/
/*         <select name="settings[basel][widget_lg_trans]" class="form-control">*/
/*         <option value="none"{% if widget_lg_trans == 'none' %} selected="selected"{% endif %}>None</option>*/
/*         <option value="uppercase"{% if widget_lg_trans == 'uppercase' %} selected="selected"{% endif %}>Uppercase</option>*/
/*         <option value="lowercase"{% if widget_lg_trans == 'lowercase' %} selected="selected"{% endif %}>Lowercase</option>*/
/*         <option value="capitalize"{% if widget_lg_trans == 'capitalize' %} selected="selected"{% endif %}>Capitalize</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Letter spcaing</label>*/
/*         <input class="form-control" name="settings[basel][widget_lg_ls]" value="{{ widget_lg_ls ? widget_lg_ls : '0px' }}" />*/
/*         </div>*/
/*     </div>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><br />Widgets in column</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="row">*/
/*         <div class="col-sm-4">*/
/*         <label>Font family</label>*/
/*         <select name="settings[basel][widget_sm_fam]" class="form-control">*/
/*         <option disabled style="font-weight:bold">System Fonts</option>*/
/*         {% for key, system_font in system_fonts %}*/
/*         <option value="{{ key }}" */
/*         {% if widget_sm_fam == key %}selected="selected"{% endif %}>*/
/*         {{ system_font }}</option>*/
/*         {% endfor %}*/
/*         <option disabled style="font-weight:bold">Google Webfonts</option>*/
/*         {% for basel_font in basel_fonts %}*/
/*         <option value="{{ basel_font.name }}" {% if widget_sm_fam == basel_font.name %}selected="selected"{% endif %}>{{ basel_font.name }}</option>*/
/*         {% endfor %}*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font size</label>*/
/*         <input class="form-control" name="settings[basel][widget_sm_size]" value="{{ widget_sm_size ? widget_sm_size : '16px' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font weight</label>*/
/*         <input class="form-control" name="settings[basel][widget_sm_weight]" value="{{ widget_sm_weight ? widget_sm_weight : '600' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Text transform</label>*/
/*         <select name="settings[basel][widget_sm_trans]" class="form-control">*/
/*         <option value="none"{% if widget_sm_trans == 'none' %} selected="selected"{% endif %}>None</option>*/
/*         <option value="uppercase"{% if widget_sm_trans == 'uppercase' %} selected="selected"{% endif %}>Uppercase</option>*/
/*         <option value="lowercase"{% if widget_sm_trans == 'lowercase' %} selected="selected"{% endif %}>Lowercase</option>*/
/*         <option value="capitalize"{% if widget_sm_trans == 'capitalize' %} selected="selected"{% endif %}>Capitalize</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Letter spcaing</label>*/
/*         <input class="form-control" name="settings[basel][widget_sm_ls]" value="{{ widget_sm_ls ? widget_sm_ls : '0.75px' }}" />*/
/*         </div>*/
/*     </div>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Main Menu In Header</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><br />Main menu</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="row">*/
/*         <div class="col-sm-4">*/
/*         <label>Font family</label>*/
/*         <select name="settings[basel][menu_font_fam]" class="form-control">*/
/*         <option disabled style="font-weight:bold">System Fonts</option>*/
/*         {% for key, system_font in system_fonts %}*/
/*         <option value="{{ key }}" */
/*         {% if menu_font_fam == key %}selected="selected"{% endif %}>*/
/*         {{ system_font }}</option>*/
/*         {% endfor %}*/
/*         <option disabled style="font-weight:bold">Google Webfonts</option>*/
/*         {% for basel_font in basel_fonts %}*/
/*         <option value="{{ basel_font.name }}" {% if menu_font_fam == basel_font.name %}selected="selected"{% endif %}>{{ basel_font.name }}</option>*/
/*         {% endfor %}*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font size</label>*/
/*         <input class="form-control" name="settings[basel][menu_font_size]" value="{{ menu_font_size ? menu_font_size : '14px' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Font weight</label>*/
/*         <input class="form-control" name="settings[basel][menu_font_weight]" value="{{ menu_font_weight ? menu_font_weight : '400' }}" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Text transform</label>*/
/*         <select name="settings[basel][menu_font_trans]" class="form-control">*/
/*         <option value="none"{% if menu_font_trans == 'none' %} selected="selected"{% endif %}>None</option>*/
/*         <option value="uppercase"{% if menu_font_trans == 'uppercase' %} selected="selected"{% endif %}>Uppercase</option>*/
/*         <option value="lowercase"{% if menu_font_trans == 'lowercase' %} selected="selected"{% endif %}>Lowercase</option>*/
/*         <option value="capitalize"{% if menu_font_trans == 'capitalize' %} selected="selected"{% endif %}>Capitalize</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Letter spcaing</label>*/
/*         <input class="form-control" name="settings[basel][menu_font_ls]" value="{{ menu_font_ls ? menu_font_ls : '0.5px' }}" />*/
/*         </div>*/
/*     </div>*/
/*     </div>                   */
/* </div> */
/* */
/* </div><!-- #basel_typo_holder ends -->*/
/* */
/* */
/* <script type="text/javascript"><!--*/
/* var font_row = {{ font_row }};*/
/* function addFontRow() {*/
/* 	html  = '<tr id="font-row' + font_row + '">';*/
/* 	html += '<td class="first">';*/
/* 	html += '<input type="text" class="form-control" name="settings[basel][basel_fonts][' + font_row + '][import]" />';*/
/* 	html += '</td>';*/
/* 	html += '<td class="first">';*/
/* 	html += '<input type="text" class="form-control" name="settings[basel][basel_fonts][' + font_row + '][name]" />';*/
/* 	html += '</td>';*/
/* 	html += '<td class="text-right"><button type="button" onclick="$(\'#font-row' + font_row  + '\').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';*/
/* 	html += '</tr>';*/
/* 	$('#fonts tbody').append(html);*/
/* 	font_row++;*/
/* }*/
/* //--></script>*/
