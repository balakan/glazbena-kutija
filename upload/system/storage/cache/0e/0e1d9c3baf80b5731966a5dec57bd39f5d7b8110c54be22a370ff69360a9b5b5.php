<?php

/* extension/basel/panel_tabs/one-click-installer.twig */
class __TwigTemplate_cf0aedcaadea2666a9e60bd96636eb4a9f4eae7b9ab30e3c4bdaa0f7c92bbf44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>One Click Installer</legend>
";
        // line 2
        if ((((isset($context["theme_default_directory"]) ? $context["theme_default_directory"] : null) == "basel") && (twig_length_filter($this->env, (isset($context["stores"]) ? $context["stores"] : null)) <= 1))) {
            // line 3
            echo "    ";
            if (array_key_exists("demos", $context)) {
                // line 4
                echo "    <div class=\"form-group\">
        <label class=\"col-sm-2 control-label\">Select Store</label>
        <div class=\"col-sm-10\">
            <select class=\"form-control\" id=\"demo-store-selector\">
\t\t\t<option value=\"0\">---- Select Store To Import ----</option>
            ";
                // line 9
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["demos"]) ? $context["demos"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["demo"]) {
                    // line 10
                    echo "                <option value=\"";
                    echo $this->getAttribute($context["demo"], "demo_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["demo"], "name", array());
                    echo "</option>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['demo'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 12
                echo "            </select>
                        
            <div class=\"demo-preview\" style=\"display:none;\"><br />
            <img id=\"demo-preview-img\" src=\"model/extension/basel/demo_stores/1/preview.jpg\" /><br /><br />
            
<div class=\"one-click-info\" style=\"margin-bottom:20px;\">
<p>By using the One click installer tool, you can easily configure your modules and layouts as our demo stores.</p>
<p>Please note that your existing layouts and module configurations will be lost when importing a demo store.</p>
<p>Sample products, categories and blog posts will be added. Existing products, categories and blog posts will not be affected.</p>
<p>* Stores in the list marked with * will add rules to the Thame Panel's Custom CSS</p>
</div>
\t\t\t<a id=\"demo-preview-btn\" class=\"btn btn-primary\" onclick=\"importDemo(1);\">Install this demo store</a>
            </div>
        </div>                   
    </div>
    ";
            }
        }
        // line 29
        echo "
";
        // line 30
        if (((isset($context["theme_default_directory"]) ? $context["theme_default_directory"] : null) != "basel")) {
            // line 31
            echo "<p>Please enable the theme before using the one click installer</p>
";
        }
        // line 33
        echo "
";
        // line 34
        if ((twig_length_filter($this->env, (isset($context["stores"]) ? $context["stores"] : null)) > 1)) {
            // line 35
            echo "<p>Since the One click installer changes layouts and modules, its not multi-store ready</p>
";
        }
        // line 37
        echo "
<script type=\"text/javascript\">
\$('select[id=\\'demo-store-selector\\']').on('change', function () {
\t
\tif (\$(this).val() == '0') {
\t\t\$('.demo-preview').css('display', 'none');
\t} else {
\t\t\$('.demo-preview').css('display', 'block');
\t}
\t
    \$('#demo-preview-img').attr('src','model/extension/basel/demo_stores/' + \$(this).val() + '/preview.jpg');
\t\$('#demo-preview-btn').attr('onclick','importDemo(' + \$(this).val() + ')');
});
var importDemo = function(store_id) {
\timporturl = '";
        // line 51
        echo (isset($context["demo_import_url"]) ? $context["demo_import_url"] : null);
        echo "'.replace(/\\&amp;/g,'&');
\tconfirm('";
        // line 52
        echo (isset($context["text_confirm"]) ? $context["text_confirm"] : null);
        echo "') ? location.href=importurl + store_id : false;
}
</script>";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/one-click-installer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 52,  102 => 51,  86 => 37,  82 => 35,  80 => 34,  77 => 33,  73 => 31,  71 => 30,  68 => 29,  49 => 12,  38 => 10,  34 => 9,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* <legend>One Click Installer</legend>*/
/* {% if theme_default_directory == 'basel' and stores|length <= 1 %}*/
/*     {% if demos is defined %}*/
/*     <div class="form-group">*/
/*         <label class="col-sm-2 control-label">Select Store</label>*/
/*         <div class="col-sm-10">*/
/*             <select class="form-control" id="demo-store-selector">*/
/* 			<option value="0">---- Select Store To Import ----</option>*/
/*             {% for demo in demos %}*/
/*                 <option value="{{ demo.demo_id }}">{{ demo.name }}</option>*/
/*             {% endfor %}*/
/*             </select>*/
/*                         */
/*             <div class="demo-preview" style="display:none;"><br />*/
/*             <img id="demo-preview-img" src="model/extension/basel/demo_stores/1/preview.jpg" /><br /><br />*/
/*             */
/* <div class="one-click-info" style="margin-bottom:20px;">*/
/* <p>By using the One click installer tool, you can easily configure your modules and layouts as our demo stores.</p>*/
/* <p>Please note that your existing layouts and module configurations will be lost when importing a demo store.</p>*/
/* <p>Sample products, categories and blog posts will be added. Existing products, categories and blog posts will not be affected.</p>*/
/* <p>* Stores in the list marked with * will add rules to the Thame Panel's Custom CSS</p>*/
/* </div>*/
/* 			<a id="demo-preview-btn" class="btn btn-primary" onclick="importDemo(1);">Install this demo store</a>*/
/*             </div>*/
/*         </div>                   */
/*     </div>*/
/*     {% endif %}*/
/* {% endif %}*/
/* */
/* {% if theme_default_directory != 'basel' %}*/
/* <p>Please enable the theme before using the one click installer</p>*/
/* {% endif %}*/
/* */
/* {% if stores|length > 1 %}*/
/* <p>Since the One click installer changes layouts and modules, its not multi-store ready</p>*/
/* {% endif %}*/
/* */
/* <script type="text/javascript">*/
/* $('select[id=\'demo-store-selector\']').on('change', function () {*/
/* 	*/
/* 	if ($(this).val() == '0') {*/
/* 		$('.demo-preview').css('display', 'none');*/
/* 	} else {*/
/* 		$('.demo-preview').css('display', 'block');*/
/* 	}*/
/* 	*/
/*     $('#demo-preview-img').attr('src','model/extension/basel/demo_stores/' + $(this).val() + '/preview.jpg');*/
/* 	$('#demo-preview-btn').attr('onclick','importDemo(' + $(this).val() + ')');*/
/* });*/
/* var importDemo = function(store_id) {*/
/* 	importurl = '{{ demo_import_url }}'.replace(/\&amp;/g,'&');*/
/* 	confirm('{{ text_confirm }}') ? location.href=importurl + store_id : false;*/
/* }*/
/* </script>*/
