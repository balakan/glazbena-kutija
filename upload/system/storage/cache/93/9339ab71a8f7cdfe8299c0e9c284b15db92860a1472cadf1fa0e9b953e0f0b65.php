<?php

/* basel/template/common/mobile-nav.twig */
class __TwigTemplate_a222e35450b937443177083c43a31ea59998e292d33c1bbdcedb8416eac42c00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"main-menu-wrapper hidden-md hidden-lg\">
<ul class=\"mobile-top\">
    <li class=\"mobile-lang-curr\"></li>
    ";
        // line 4
        if ((isset($context["header_search"]) ? $context["header_search"] : null)) {
            // line 5
            echo "    <li class=\"search\">
        <div class=\"search-holder-mobile\">
        <input type=\"text\" name=\"search-mobile\" value=\"\" placeholder=\"\" class=\"form-control\" /><a class=\"fa fa-search\"></a>
        </div>
    </li>
    ";
        }
        // line 11
        echo "</ul>
";
        // line 12
        if ((isset($context["primary_menu"]) ? $context["primary_menu"] : null)) {
            // line 13
            echo "<ul class=\"categories\">
";
            // line 14
            if (((isset($context["primary_menu"]) ? $context["primary_menu"] : null) == "oc")) {
                // line 15
                echo "<!-- Default menu -->
";
                // line 16
                echo (isset($context["default_menu"]) ? $context["default_menu"] : null);
                echo "
";
            } elseif (            // line 17
array_key_exists("primary_menu", $context)) {
                // line 18
                echo "<!-- Mega menu -->
";
                // line 19
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["primary_menu_mobile"]) ? $context["primary_menu_mobile"] : null));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["key"] => $context["row"]) {
                    // line 20
                    $this->loadTemplate("basel/template/common/menus/mega_menu.twig", "basel/template/common/mobile-nav.twig", 20)->display($context);
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 23
            echo "</ul>
";
        }
        // line 25
        if ((isset($context["secondary_menu"]) ? $context["secondary_menu"] : null)) {
            // line 26
            echo "<ul class=\"categories\">
    ";
            // line 27
            if (((isset($context["secondary_menu"]) ? $context["secondary_menu"] : null) == "oc")) {
                // line 28
                echo "        <!-- Default menu -->
        ";
                // line 29
                echo (isset($context["default_menu"]) ? $context["default_menu"] : null);
                echo "
    ";
            } elseif (            // line 30
array_key_exists("secondary_menu", $context)) {
                // line 31
                echo "        <!-- Mega menu -->
        ";
                // line 32
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["secondary_menu_mobile"]) ? $context["secondary_menu_mobile"] : null));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["key"] => $context["row"]) {
                    // line 33
                    echo "        \t";
                    $this->loadTemplate("basel/template/common/menus/mega_menu.twig", "basel/template/common/mobile-nav.twig", 33)->display($context);
                    // line 34
                    echo "        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 35
                echo "    ";
            }
            // line 36
            echo "</ul>
";
        }
        // line 38
        echo "<ul class=\"categories\">
    ";
        // line 39
        $this->loadTemplate("basel/template/common/static_links.twig", "basel/template/common/mobile-nav.twig", 39)->display($context);
        // line 40
        echo "</ul>
</div>
<span class=\"body-cover menu-closer\"></span>";
    }

    public function getTemplateName()
    {
        return "basel/template/common/mobile-nav.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 40,  155 => 39,  152 => 38,  148 => 36,  145 => 35,  131 => 34,  128 => 33,  111 => 32,  108 => 31,  106 => 30,  102 => 29,  99 => 28,  97 => 27,  94 => 26,  92 => 25,  88 => 23,  73 => 20,  56 => 19,  53 => 18,  51 => 17,  47 => 16,  44 => 15,  42 => 14,  39 => 13,  37 => 12,  34 => 11,  26 => 5,  24 => 4,  19 => 1,);
    }
}
/* <div class="main-menu-wrapper hidden-md hidden-lg">*/
/* <ul class="mobile-top">*/
/*     <li class="mobile-lang-curr"></li>*/
/*     {% if header_search %}*/
/*     <li class="search">*/
/*         <div class="search-holder-mobile">*/
/*         <input type="text" name="search-mobile" value="" placeholder="" class="form-control" /><a class="fa fa-search"></a>*/
/*         </div>*/
/*     </li>*/
/*     {% endif %}*/
/* </ul>*/
/* {% if primary_menu %}*/
/* <ul class="categories">*/
/* {% if primary_menu == 'oc' %}*/
/* <!-- Default menu -->*/
/* {{ default_menu }}*/
/* {% elseif primary_menu is defined %}*/
/* <!-- Mega menu -->*/
/* {% for key, row in primary_menu_mobile %}*/
/* {% include 'basel/template/common/menus/mega_menu.twig' %}*/
/* {% endfor %}*/
/* {% endif %}*/
/* </ul>*/
/* {% endif %}*/
/* {% if secondary_menu %}*/
/* <ul class="categories">*/
/*     {% if secondary_menu == 'oc' %}*/
/*         <!-- Default menu -->*/
/*         {{ default_menu }}*/
/*     {% elseif secondary_menu is defined %}*/
/*         <!-- Mega menu -->*/
/*         {% for key, row in secondary_menu_mobile %}*/
/*         	{% include 'basel/template/common/menus/mega_menu.twig' %}*/
/*         {% endfor %}*/
/*     {% endif %}*/
/* </ul>*/
/* {% endif %}*/
/* <ul class="categories">*/
/*     {% include 'basel/template/common/static_links.twig' %}*/
/* </ul>*/
/* </div>*/
/* <span class="body-cover menu-closer"></span>*/
