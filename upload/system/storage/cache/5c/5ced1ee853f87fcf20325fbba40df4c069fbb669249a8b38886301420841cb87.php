<?php

/* extension/basel/panel_tabs/page-titles.twig */
class __TwigTemplate_51559903708e0263e798ef759e973c7f7546251b5010d6339aeaa4b1a689cc13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Breadcrumbs / Page Titles</legend>
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Select page title style on product listing pages, like category pages, search result page etc\">Product listing pages</span></label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_titles_listings]\" class=\"form-control\">
        <option value=\"default_bc\"
        ";
        // line 7
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "default_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc\"
        ";
        // line 11
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "default_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Full Width
        </option>
        <option value=\"default_bc normal_height_bc\"
        ";
        // line 15
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "default_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc normal_height_bc\"
        ";
        // line 19
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "default_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Full Width
        </option>
        <option value=\"minimal_bc\"
        ";
        // line 23
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "minimal_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc\"
        ";
        // line 27
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "minimal_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Full Width
        </option>
        <option value=\"minimal_bc normal_height_bc\"
        ";
        // line 31
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "minimal_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc normal_height_bc\"
        ";
        // line 35
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "minimal_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Full Width
        </option>
        <option value=\"minimal_inline_bc\"
        ";
        // line 39
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "minimal_inline_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Short Height
        </option>
        <option value=\"minimal_inline_bc normal_height_bc\"
        ";
        // line 43
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "minimal_inline_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Normal Height
        </option>
        <option value=\"title_in_bc\"";
        // line 46
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "title_in_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Short Height
        </option>
        <option value=\"title_in_bc normal_height_bc\"";
        // line 49
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "title_in_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Normal Height
        </option>
        <option value=\"title_in_bc tall_height_bc\"";
        // line 52
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "title_in_bc tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Tall Height
        </option>
        <option value=\"title_in_bc extra_tall_height_bc\"";
        // line 55
        if (((isset($context["basel_titles_listings"]) ? $context["basel_titles_listings"] : null) == "title_in_bc extra_tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Extra Tall Height
        </option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Product pages</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_titles_product]\" class=\"form-control\">
        <option value=\"default_bc\"
        ";
        // line 67
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "default_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc\"
        ";
        // line 71
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "default_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Full Width
        </option>
        <option value=\"default_bc normal_height_bc\"
        ";
        // line 75
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "default_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc normal_height_bc\"
        ";
        // line 79
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "default_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Full Width
        </option>
        <option value=\"minimal_bc\"
        ";
        // line 83
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "minimal_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc\"
        ";
        // line 87
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "minimal_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Full Width
        </option>
        <option value=\"minimal_bc normal_height_bc\"
        ";
        // line 91
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "minimal_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc normal_height_bc\"
        ";
        // line 95
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "minimal_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Full Width
        </option>
        <option value=\"minimal_inline_bc\"
        ";
        // line 99
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "minimal_inline_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Short Height
        </option>
        <option value=\"minimal_inline_bc normal_height_bc\"
        ";
        // line 103
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "minimal_inline_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Normal Height
        </option>
        <option value=\"title_in_bc\"";
        // line 106
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "title_in_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Short Height
        </option>
        <option value=\"title_in_bc normal_height_bc\"";
        // line 109
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "title_in_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Normal Height
        </option>
        <option value=\"title_in_bc tall_height_bc\"";
        // line 112
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "title_in_bc tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Tall Height
        </option>
        <option value=\"title_in_bc extra_tall_height_bc\"";
        // line 115
        if (((isset($context["basel_titles_product"]) ? $context["basel_titles_product"] : null) == "title_in_bc extra_tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Extra Tall Height
        </option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Select page title style on account and affiliate pages\">Account pages</span></label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_titles_account]\" class=\"form-control\">
        <option value=\"default_bc\"
        ";
        // line 127
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "default_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc\"
        ";
        // line 131
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "default_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Full Width
        </option>
        <option value=\"default_bc normal_height_bc\"
        ";
        // line 135
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "default_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc normal_height_bc\"
        ";
        // line 139
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "default_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Full Width
        </option>
        <option value=\"minimal_bc\"
        ";
        // line 143
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "minimal_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc\"
        ";
        // line 147
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "minimal_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Full Width
        </option>
        <option value=\"minimal_bc normal_height_bc\"
        ";
        // line 151
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "minimal_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc normal_height_bc\"
        ";
        // line 155
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "minimal_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Full Width
        </option>
        <option value=\"minimal_inline_bc\"
        ";
        // line 159
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "minimal_inline_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Short Height
        </option>
        <option value=\"minimal_inline_bc normal_height_bc\"
        ";
        // line 163
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "minimal_inline_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Normal Height
        </option>
        <option value=\"title_in_bc\"";
        // line 166
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "title_in_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Short Height
        </option>
        <option value=\"title_in_bc normal_height_bc\"";
        // line 169
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "title_in_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Normal Height
        </option>
        <option value=\"title_in_bc tall_height_bc\"";
        // line 172
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "title_in_bc tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Tall Height
        </option>
        <option value=\"title_in_bc extra_tall_height_bc\"";
        // line 175
        if (((isset($context["basel_titles_account"]) ? $context["basel_titles_account"] : null) == "title_in_bc extra_tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Extra Tall Height
        </option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Select page title style on shopping cart and checkout pages\">Checkout</span></label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_titles_checkout]\" class=\"form-control\">
        <option value=\"default_bc\"
        ";
        // line 187
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "default_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc\"
        ";
        // line 191
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "default_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Full Width
        </option>
        <option value=\"default_bc normal_height_bc\"
        ";
        // line 195
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "default_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc normal_height_bc\"
        ";
        // line 199
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "default_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Full Width
        </option>
        <option value=\"minimal_bc\"
        ";
        // line 203
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "minimal_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc\"
        ";
        // line 207
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "minimal_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Full Width
        </option>
        <option value=\"minimal_bc normal_height_bc\"
        ";
        // line 211
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "minimal_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc normal_height_bc\"
        ";
        // line 215
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "minimal_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Full Width
        </option>
        <option value=\"minimal_inline_bc\"
        ";
        // line 219
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "minimal_inline_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Short Height
        </option>
        <option value=\"minimal_inline_bc normal_height_bc\"
        ";
        // line 223
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "minimal_inline_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Normal Height
        </option>
        <option value=\"title_in_bc\"";
        // line 226
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "title_in_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Short Height
        </option>
        <option value=\"title_in_bc normal_height_bc\"";
        // line 229
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "title_in_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Normal Height
        </option>
        <option value=\"title_in_bc tall_height_bc\"";
        // line 232
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "title_in_bc tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Tall Height
        </option>
        <option value=\"title_in_bc extra_tall_height_bc\"";
        // line 235
        if (((isset($context["basel_titles_checkout"]) ? $context["basel_titles_checkout"] : null) == "title_in_bc extra_tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Extra Tall Height
        </option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Contact page</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_titles_contact]\" class=\"form-control\">
        <option value=\"default_bc\"
        ";
        // line 247
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "default_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc\"
        ";
        // line 251
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "default_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Full Width
        </option>
        <option value=\"default_bc normal_height_bc\"
        ";
        // line 255
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "default_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc normal_height_bc\"
        ";
        // line 259
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "default_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Full Width
        </option>
        <option value=\"minimal_bc\"
        ";
        // line 263
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "minimal_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc\"
        ";
        // line 267
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "minimal_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Full Width
        </option>
        <option value=\"minimal_bc normal_height_bc\"
        ";
        // line 271
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "minimal_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc normal_height_bc\"
        ";
        // line 275
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "minimal_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Full Width
        </option>
        <option value=\"minimal_inline_bc\"
        ";
        // line 279
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "minimal_inline_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Short Height
        </option>
        <option value=\"minimal_inline_bc normal_height_bc\"
        ";
        // line 283
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "minimal_inline_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Normal Height
        </option>
        <option value=\"title_in_bc\"";
        // line 286
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "title_in_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Short Height
        </option>
        <option value=\"title_in_bc normal_height_bc\"";
        // line 289
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "title_in_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Normal Height
        </option>
        <option value=\"title_in_bc tall_height_bc\"";
        // line 292
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "title_in_bc tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Tall Height
        </option>
        <option value=\"title_in_bc extra_tall_height_bc\"";
        // line 295
        if (((isset($context["basel_titles_contact"]) ? $context["basel_titles_contact"] : null) == "title_in_bc extra_tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Extra Tall Height
        </option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Blog pages</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_titles_blog]\" class=\"form-control\">
        <option value=\"default_bc\"
        ";
        // line 307
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "default_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc\"
        ";
        // line 311
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "default_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Full Width
        </option>
        <option value=\"default_bc normal_height_bc\"
        ";
        // line 315
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "default_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc normal_height_bc\"
        ";
        // line 319
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "default_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Full Width
        </option>
        <option value=\"minimal_bc\"
        ";
        // line 323
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "minimal_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc\"
        ";
        // line 327
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "minimal_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Full Width
        </option>
        <option value=\"minimal_bc normal_height_bc\"
        ";
        // line 331
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "minimal_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc normal_height_bc\"
        ";
        // line 335
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "minimal_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Full Width
        </option>
        <option value=\"minimal_inline_bc\"
        ";
        // line 339
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "minimal_inline_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Short Height
        </option>
        <option value=\"minimal_inline_bc normal_height_bc\"
        ";
        // line 343
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "minimal_inline_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Normal Height
        </option>
        <option value=\"title_in_bc\"";
        // line 346
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "title_in_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Short Height
        </option>
        <option value=\"title_in_bc normal_height_bc\"";
        // line 349
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "title_in_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Normal Height
        </option>
        <option value=\"title_in_bc tall_height_bc\"";
        // line 352
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "title_in_bc tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Tall Height
        </option>
        <option value=\"title_in_bc extra_tall_height_bc\"";
        // line 355
        if (((isset($context["basel_titles_blog"]) ? $context["basel_titles_blog"] : null) == "title_in_bc extra_tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Extra Tall Height
        </option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Select page title style on information pages. This style will also work as default fall back style\">Catalog pages</span></label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_titles_default]\" class=\"form-control\">
        <option value=\"default_bc\"
        ";
        // line 367
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "default_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc\"
        ";
        // line 371
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "default_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style + Short Height + Full Width
        </option>
        <option value=\"default_bc normal_height_bc\"
        ";
        // line 375
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "default_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Boxed Width
        </option>
        <option value=\"default_bc full_width_bc normal_height_bc\"
        ";
        // line 379
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "default_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Default Style / Normal Height + Full Width
        </option>
        <option value=\"minimal_bc\"
        ";
        // line 383
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "minimal_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc\"
        ";
        // line 387
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "minimal_bc full_width_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Short Height + Full Width
        </option>
        <option value=\"minimal_bc normal_height_bc\"
        ";
        // line 391
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "minimal_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Boxed Width
        </option>
        <option value=\"minimal_bc full_width_bc normal_height_bc\"
        ";
        // line 395
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "minimal_bc full_width_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal + Normal Height + Full Width
        </option>
        <option value=\"minimal_inline_bc\"
        ";
        // line 399
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "minimal_inline_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Short Height
        </option>
        <option value=\"minimal_inline_bc normal_height_bc\"
        ";
        // line 403
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "minimal_inline_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Minimal Inline + Normal Height
        </option>
        <option value=\"title_in_bc\"";
        // line 406
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "title_in_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Short Height
        </option>
        <option value=\"title_in_bc normal_height_bc\"";
        // line 409
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "title_in_bc normal_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Normal Height
        </option>
        <option value=\"title_in_bc tall_height_bc\"";
        // line 412
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "title_in_bc tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Tall Height
        </option>
        <option value=\"title_in_bc extra_tall_height_bc\"";
        // line 415
        if (((isset($context["basel_titles_default"]) ? $context["basel_titles_default"] : null) == "title_in_bc extra_tall_height_bc")) {
            echo " selected=\"selected\"";
        }
        echo ">
        Page Title Inside Breadcrumb + Extra Tall Height
        </option>
    </select>
</div>                   
</div>

<legend>Back button</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Adds a back button in the breadcrumbs\">Back button</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_back_btn]\" value=\"0\" ";
        // line 426
        if (((isset($context["basel_back_btn"]) ? $context["basel_back_btn"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_back_btn]\" value=\"1\" ";
        // line 427
        if (((isset($context["basel_back_btn"]) ? $context["basel_back_btn"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>


";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/page-titles.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  942 => 427,  936 => 426,  920 => 415,  912 => 412,  904 => 409,  896 => 406,  888 => 403,  879 => 399,  870 => 395,  861 => 391,  852 => 387,  843 => 383,  834 => 379,  825 => 375,  816 => 371,  807 => 367,  790 => 355,  782 => 352,  774 => 349,  766 => 346,  758 => 343,  749 => 339,  740 => 335,  731 => 331,  722 => 327,  713 => 323,  704 => 319,  695 => 315,  686 => 311,  677 => 307,  660 => 295,  652 => 292,  644 => 289,  636 => 286,  628 => 283,  619 => 279,  610 => 275,  601 => 271,  592 => 267,  583 => 263,  574 => 259,  565 => 255,  556 => 251,  547 => 247,  530 => 235,  522 => 232,  514 => 229,  506 => 226,  498 => 223,  489 => 219,  480 => 215,  471 => 211,  462 => 207,  453 => 203,  444 => 199,  435 => 195,  426 => 191,  417 => 187,  400 => 175,  392 => 172,  384 => 169,  376 => 166,  368 => 163,  359 => 159,  350 => 155,  341 => 151,  332 => 147,  323 => 143,  314 => 139,  305 => 135,  296 => 131,  287 => 127,  270 => 115,  262 => 112,  254 => 109,  246 => 106,  238 => 103,  229 => 99,  220 => 95,  211 => 91,  202 => 87,  193 => 83,  184 => 79,  175 => 75,  166 => 71,  157 => 67,  140 => 55,  132 => 52,  124 => 49,  116 => 46,  108 => 43,  99 => 39,  90 => 35,  81 => 31,  72 => 27,  63 => 23,  54 => 19,  45 => 15,  36 => 11,  27 => 7,  19 => 1,);
    }
}
/* <legend>Breadcrumbs / Page Titles</legend>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Select page title style on product listing pages, like category pages, search result page etc">Product listing pages</span></label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_titles_listings]" class="form-control">*/
/*         <option value="default_bc"*/
/*         {% if basel_titles_listings == 'default_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc"*/
/*         {% if basel_titles_listings == 'default_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Full Width*/
/*         </option>*/
/*         <option value="default_bc normal_height_bc"*/
/*         {% if basel_titles_listings == 'default_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_listings == 'default_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc"*/
/*         {% if basel_titles_listings == 'minimal_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc"*/
/*         {% if basel_titles_listings == 'minimal_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc normal_height_bc"*/
/*         {% if basel_titles_listings == 'minimal_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_listings == 'minimal_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_inline_bc"*/
/*         {% if basel_titles_listings == 'minimal_inline_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Short Height*/
/*         </option>*/
/*         <option value="minimal_inline_bc normal_height_bc"*/
/*         {% if basel_titles_listings == 'minimal_inline_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc"{% if basel_titles_listings == 'title_in_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Short Height*/
/*         </option>*/
/*         <option value="title_in_bc normal_height_bc"{% if basel_titles_listings == 'title_in_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc tall_height_bc"{% if basel_titles_listings == 'title_in_bc tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Tall Height*/
/*         </option>*/
/*         <option value="title_in_bc extra_tall_height_bc"{% if basel_titles_listings == 'title_in_bc extra_tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Extra Tall Height*/
/*         </option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Product pages</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_titles_product]" class="form-control">*/
/*         <option value="default_bc"*/
/*         {% if basel_titles_product == 'default_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc"*/
/*         {% if basel_titles_product == 'default_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Full Width*/
/*         </option>*/
/*         <option value="default_bc normal_height_bc"*/
/*         {% if basel_titles_product == 'default_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_product == 'default_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc"*/
/*         {% if basel_titles_product == 'minimal_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc"*/
/*         {% if basel_titles_product == 'minimal_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc normal_height_bc"*/
/*         {% if basel_titles_product == 'minimal_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_product == 'minimal_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_inline_bc"*/
/*         {% if basel_titles_product == 'minimal_inline_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Short Height*/
/*         </option>*/
/*         <option value="minimal_inline_bc normal_height_bc"*/
/*         {% if basel_titles_product == 'minimal_inline_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc"{% if basel_titles_product == 'title_in_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Short Height*/
/*         </option>*/
/*         <option value="title_in_bc normal_height_bc"{% if basel_titles_product == 'title_in_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc tall_height_bc"{% if basel_titles_product == 'title_in_bc tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Tall Height*/
/*         </option>*/
/*         <option value="title_in_bc extra_tall_height_bc"{% if basel_titles_product == 'title_in_bc extra_tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Extra Tall Height*/
/*         </option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Select page title style on account and affiliate pages">Account pages</span></label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_titles_account]" class="form-control">*/
/*         <option value="default_bc"*/
/*         {% if basel_titles_account == 'default_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc"*/
/*         {% if basel_titles_account == 'default_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Full Width*/
/*         </option>*/
/*         <option value="default_bc normal_height_bc"*/
/*         {% if basel_titles_account == 'default_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_account == 'default_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc"*/
/*         {% if basel_titles_account == 'minimal_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc"*/
/*         {% if basel_titles_account == 'minimal_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc normal_height_bc"*/
/*         {% if basel_titles_account == 'minimal_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_account == 'minimal_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_inline_bc"*/
/*         {% if basel_titles_account == 'minimal_inline_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Short Height*/
/*         </option>*/
/*         <option value="minimal_inline_bc normal_height_bc"*/
/*         {% if basel_titles_account == 'minimal_inline_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc"{% if basel_titles_account == 'title_in_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Short Height*/
/*         </option>*/
/*         <option value="title_in_bc normal_height_bc"{% if basel_titles_account == 'title_in_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc tall_height_bc"{% if basel_titles_account == 'title_in_bc tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Tall Height*/
/*         </option>*/
/*         <option value="title_in_bc extra_tall_height_bc"{% if basel_titles_account == 'title_in_bc extra_tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Extra Tall Height*/
/*         </option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Select page title style on shopping cart and checkout pages">Checkout</span></label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_titles_checkout]" class="form-control">*/
/*         <option value="default_bc"*/
/*         {% if basel_titles_checkout == 'default_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc"*/
/*         {% if basel_titles_checkout == 'default_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Full Width*/
/*         </option>*/
/*         <option value="default_bc normal_height_bc"*/
/*         {% if basel_titles_checkout == 'default_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_checkout == 'default_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc"*/
/*         {% if basel_titles_checkout == 'minimal_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc"*/
/*         {% if basel_titles_checkout == 'minimal_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc normal_height_bc"*/
/*         {% if basel_titles_checkout == 'minimal_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_checkout == 'minimal_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_inline_bc"*/
/*         {% if basel_titles_checkout == 'minimal_inline_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Short Height*/
/*         </option>*/
/*         <option value="minimal_inline_bc normal_height_bc"*/
/*         {% if basel_titles_checkout == 'minimal_inline_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc"{% if basel_titles_checkout == 'title_in_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Short Height*/
/*         </option>*/
/*         <option value="title_in_bc normal_height_bc"{% if basel_titles_checkout == 'title_in_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc tall_height_bc"{% if basel_titles_checkout == 'title_in_bc tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Tall Height*/
/*         </option>*/
/*         <option value="title_in_bc extra_tall_height_bc"{% if basel_titles_checkout == 'title_in_bc extra_tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Extra Tall Height*/
/*         </option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Contact page</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_titles_contact]" class="form-control">*/
/*         <option value="default_bc"*/
/*         {% if basel_titles_contact == 'default_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc"*/
/*         {% if basel_titles_contact == 'default_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Full Width*/
/*         </option>*/
/*         <option value="default_bc normal_height_bc"*/
/*         {% if basel_titles_contact == 'default_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_contact == 'default_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc"*/
/*         {% if basel_titles_contact == 'minimal_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc"*/
/*         {% if basel_titles_contact == 'minimal_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc normal_height_bc"*/
/*         {% if basel_titles_contact == 'minimal_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_contact == 'minimal_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_inline_bc"*/
/*         {% if basel_titles_contact == 'minimal_inline_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Short Height*/
/*         </option>*/
/*         <option value="minimal_inline_bc normal_height_bc"*/
/*         {% if basel_titles_contact == 'minimal_inline_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc"{% if basel_titles_contact == 'title_in_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Short Height*/
/*         </option>*/
/*         <option value="title_in_bc normal_height_bc"{% if basel_titles_contact == 'title_in_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc tall_height_bc"{% if basel_titles_contact == 'title_in_bc tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Tall Height*/
/*         </option>*/
/*         <option value="title_in_bc extra_tall_height_bc"{% if basel_titles_contact == 'title_in_bc extra_tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Extra Tall Height*/
/*         </option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Blog pages</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_titles_blog]" class="form-control">*/
/*         <option value="default_bc"*/
/*         {% if basel_titles_blog == 'default_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc"*/
/*         {% if basel_titles_blog == 'default_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Full Width*/
/*         </option>*/
/*         <option value="default_bc normal_height_bc"*/
/*         {% if basel_titles_blog == 'default_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_blog == 'default_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc"*/
/*         {% if basel_titles_blog == 'minimal_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc"*/
/*         {% if basel_titles_blog == 'minimal_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc normal_height_bc"*/
/*         {% if basel_titles_blog == 'minimal_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_blog == 'minimal_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_inline_bc"*/
/*         {% if basel_titles_blog == 'minimal_inline_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Short Height*/
/*         </option>*/
/*         <option value="minimal_inline_bc normal_height_bc"*/
/*         {% if basel_titles_blog == 'minimal_inline_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc"{% if basel_titles_blog == 'title_in_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Short Height*/
/*         </option>*/
/*         <option value="title_in_bc normal_height_bc"{% if basel_titles_blog == 'title_in_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc tall_height_bc"{% if basel_titles_blog == 'title_in_bc tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Tall Height*/
/*         </option>*/
/*         <option value="title_in_bc extra_tall_height_bc"{% if basel_titles_blog == 'title_in_bc extra_tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Extra Tall Height*/
/*         </option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Select page title style on information pages. This style will also work as default fall back style">Catalog pages</span></label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_titles_default]" class="form-control">*/
/*         <option value="default_bc"*/
/*         {% if basel_titles_default == 'default_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc"*/
/*         {% if basel_titles_default == 'default_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Default Style + Short Height + Full Width*/
/*         </option>*/
/*         <option value="default_bc normal_height_bc"*/
/*         {% if basel_titles_default == 'default_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="default_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_default == 'default_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Default Style / Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc"*/
/*         {% if basel_titles_default == 'minimal_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc"*/
/*         {% if basel_titles_default == 'minimal_bc full_width_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Short Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_bc normal_height_bc"*/
/*         {% if basel_titles_default == 'minimal_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Boxed Width*/
/*         </option>*/
/*         <option value="minimal_bc full_width_bc normal_height_bc"*/
/*         {% if basel_titles_default == 'minimal_bc full_width_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal + Normal Height + Full Width*/
/*         </option>*/
/*         <option value="minimal_inline_bc"*/
/*         {% if basel_titles_default == 'minimal_inline_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Short Height*/
/*         </option>*/
/*         <option value="minimal_inline_bc normal_height_bc"*/
/*         {% if basel_titles_default == 'minimal_inline_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Minimal Inline + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc"{% if basel_titles_default == 'title_in_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Short Height*/
/*         </option>*/
/*         <option value="title_in_bc normal_height_bc"{% if basel_titles_default == 'title_in_bc normal_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Normal Height*/
/*         </option>*/
/*         <option value="title_in_bc tall_height_bc"{% if basel_titles_default == 'title_in_bc tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Tall Height*/
/*         </option>*/
/*         <option value="title_in_bc extra_tall_height_bc"{% if basel_titles_default == 'title_in_bc extra_tall_height_bc' %} selected="selected"{% endif %}>*/
/*         Page Title Inside Breadcrumb + Extra Tall Height*/
/*         </option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <legend>Back button</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Adds a back button in the breadcrumbs">Back button</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_back_btn]" value="0" {% if basel_back_btn == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_back_btn]" value="1" {% if basel_back_btn == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* */
/* */
