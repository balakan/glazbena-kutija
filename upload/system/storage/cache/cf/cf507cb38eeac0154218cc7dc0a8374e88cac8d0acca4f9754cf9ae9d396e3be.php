<?php

/* basel/template/common/static_links.twig */
class __TwigTemplate_a1138f2d109cd1a7de39242f876fb2d272598fb5c112024cd082c1a37cffcad8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["use_custom_links"]) ? $context["use_custom_links"] : null)) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["basel_links"]) ? $context["basel_links"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["basel_link"]) {
                // line 3
                echo "<li class=\"static-link\"><a class=\"anim-underline\" href=\"";
                echo $this->getAttribute($context["basel_link"], "target", array());
                echo "\">";
                echo $this->getAttribute($context["basel_link"], "text", array());
                echo "</a></li>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_link'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 6
            echo "<li class=\"static-link\"><a class=\"anim-underline\"  href=\"";
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\" title=\"";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "\">";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>
<li class=\"static-link is_wishlist\"><a class=\"anim-underline wishlist-total\" href=\"";
            // line 7
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\" title=\"";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "\"><span>";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "</span></a></li>
<li class=\"static-link\"><a class=\"anim-underline\"  href=\"";
            // line 8
            echo (isset($context["shopping_cart"]) ? $context["shopping_cart"] : null);
            echo "\" title=\"";
            echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
            echo "\">";
            echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
            echo "</a></li>
<li class=\"static-link\"><a class=\"anim-underline\"  href=\"";
            // line 9
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo "\" title=\"";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo "\">";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo "</a></li>
";
        }
    }

    public function getTemplateName()
    {
        return "basel/template/common/static_links.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 9,  54 => 8,  46 => 7,  37 => 6,  25 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if use_custom_links %}*/
/* {% for basel_link in basel_links %}*/
/* <li class="static-link"><a class="anim-underline" href="{{ basel_link.target }}">{{ basel_link.text }}</a></li>*/
/* {% endfor %}*/
/* {% else %}*/
/* <li class="static-link"><a class="anim-underline"  href="{{ account }}" title="{{ text_account }}">{{ text_account }}</a></li>*/
/* <li class="static-link is_wishlist"><a class="anim-underline wishlist-total" href="{{ wishlist }}" title="{{ text_wishlist }}"><span>{{ text_wishlist }}</span></a></li>*/
/* <li class="static-link"><a class="anim-underline"  href="{{ shopping_cart }}" title="{{ text_shopping_cart }}">{{ text_shopping_cart }}</a></li>*/
/* <li class="static-link"><a class="anim-underline"  href="{{ checkout }}" title="{{ text_checkout }}">{{ text_checkout }}</a></li>*/
/* {% endif %}*/
