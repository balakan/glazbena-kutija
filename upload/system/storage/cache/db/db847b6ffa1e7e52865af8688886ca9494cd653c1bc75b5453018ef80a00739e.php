<?php

/* extension/basel/panel_tabs/header.twig */
class __TwigTemplate_cf6754ef1f6d0728d7cffa2e50d6cf36d4cb66e16c8d53be1c7b46eed2d53ec2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Header Styling</legend>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Header Alternative</label>
<div class=\"col-sm-10\">
    <select id=\"header-select\" name=\"settings[basel][basel_header]\" class=\"form-control\">
        <option value=\"header1\"";
        // line 7
        if (((isset($context["basel_header"]) ? $context["basel_header"] : null) == "header1")) {
            echo " selected=\"selected\"";
        }
        echo ">Header 1</option>
        <option value=\"header2\"";
        // line 8
        if (((isset($context["basel_header"]) ? $context["basel_header"] : null) == "header2")) {
            echo " selected=\"selected\"";
        }
        echo ">Header 2</option>
        <option value=\"header3\"";
        // line 9
        if (((isset($context["basel_header"]) ? $context["basel_header"] : null) == "header3")) {
            echo " selected=\"selected\"";
        }
        echo ">Header 3</option>
        <option value=\"header4\"";
        // line 10
        if (((isset($context["basel_header"]) ? $context["basel_header"] : null) == "header4")) {
            echo " selected=\"selected\"";
        }
        echo ">Header 4</option>
        <option value=\"header5\"";
        // line 11
        if (((isset($context["basel_header"]) ? $context["basel_header"] : null) == "header5")) {
            echo " selected=\"selected\"";
        }
        echo ">Header 5</option>
        <option value=\"header6\"";
        // line 12
        if (((isset($context["basel_header"]) ? $context["basel_header"] : null) == "header6")) {
            echo " selected=\"selected\"";
        }
        echo ">Header 6</option>
    </select>
    <div id=\"header-preview\" class=\"preview-holder\">
    <img src=\"view/javascript/basel/img/theme-panel/headers/";
        // line 15
        echo (isset($context["basel_header"]) ? $context["basel_header"] : null);
        echo ".png\" />
    </div>
</div>
</div>

<legend class=\"sub\">Top Line</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Top Line Status</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][top_line_style]\" value=\"0\" ";
        // line 25
        if (((isset($context["top_line_style"]) ? $context["top_line_style"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][top_line_style]\" value=\"1\" ";
        // line 26
        if (((isset($context["top_line_style"]) ? $context["top_line_style"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Top Line Width</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][top_line_width]\" class=\"form-control\">
        <option value=\"full-width\"";
        // line 34
        if (((isset($context["top_line_width"]) ? $context["top_line_width"] : null) == "full-width")) {
            echo " selected=\"selected\"";
        }
        echo ">Full Width</option>
        <option value=\"boxed\"";
        // line 35
        if (((isset($context["top_line_width"]) ? $context["top_line_width"] : null) == "boxed")) {
            echo " selected=\"selected\"";
        }
        echo ">Boxed</option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enter value in pixels, eg. 41\">Top Line Height</span></label>
<div class=\"col-sm-10\">
    <input class=\"form-control\" name=\"settings[basel][top_line_height]\" value=\"";
        // line 43
        echo (((isset($context["top_line_height"]) ? $context["top_line_height"] : null)) ? ((isset($context["top_line_height"]) ? $context["top_line_height"] : null)) : ("41"));
        echo "\" />
</div>                   
</div>

<legend class=\"sub\">Main Header Area</legend>
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Main Area Width</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][main_header_width]\" class=\"form-control\">
        <option value=\"full-width\"";
        // line 52
        if (((isset($context["main_header_width"]) ? $context["main_header_width"] : null) == "full-width")) {
            echo " selected=\"selected\"";
        }
        echo ">Full Width</option>
        <option value=\"boxed\"";
        // line 53
        if (((isset($context["main_header_width"]) ? $context["main_header_width"] : null) == "boxed")) {
            echo " selected=\"selected\"";
        }
        echo ">Boxed</option>
    </select>
</div>                   
</div>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><br /><span data-toggle=\"tooltip\" title=\"Enter values in pixels, eg. 100\">Main Header Area Height</span></label>
    <div class=\"col-sm-10\">
    
    <div class=\"row\">
    <div class=\"col-sm-4\">
    <label>Desktop View</label>
    <input class=\"form-control\" name=\"settings[basel][main_header_height]\" value=\"";
        // line 64
        echo (((isset($context["main_header_height"]) ? $context["main_header_height"] : null)) ? ((isset($context["main_header_height"]) ? $context["main_header_height"] : null)) : ("104"));
        echo "\" />
    </div>
    
    <div class=\"col-sm-4\">
    <label>Mobile View</label>
    <input class=\"form-control\" name=\"settings[basel][main_header_height_mobile]\" value=\"";
        // line 69
        echo (((isset($context["main_header_height_mobile"]) ? $context["main_header_height_mobile"] : null)) ? ((isset($context["main_header_height_mobile"]) ? $context["main_header_height_mobile"] : null)) : ("70"));
        echo "\" />
    </div>
    
    <div class=\"col-sm-4\">
    <label>Sticky Header</label>
    <input class=\"form-control\" name=\"settings[basel][main_header_height_sticky]\" value=\"";
        // line 74
        echo (((isset($context["main_header_height_sticky"]) ? $context["main_header_height_sticky"] : null)) ? ((isset($context["main_header_height_sticky"]) ? $context["main_header_height_sticky"] : null)) : ("70"));
        echo "\" />
    </div>
        
    </div>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Keep the header fixed on top of the page when scrolling\">Sticky Header</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_sticky_header]\" value=\"0\" ";
        // line 84
        if (((isset($context["basel_sticky_header"]) ? $context["basel_sticky_header"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_sticky_header]\" value=\"1\" ";
        // line 85
        if (((isset($context["basel_sticky_header"]) ? $context["basel_sticky_header"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<legend class=\"sub\">Logo</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enter value in pixels, eg. 100\">Logo Max Width</span></label>
    <div class=\"col-sm-10\">
    <input class=\"form-control\" name=\"settings[basel][logo_maxwidth]\" value=\"";
        // line 93
        echo (((isset($context["logo_maxwidth"]) ? $context["logo_maxwidth"] : null)) ? ((isset($context["logo_maxwidth"]) ? $context["logo_maxwidth"] : null)) : ("250"));
        echo "\" />        
    </div>
</div>

<legend class=\"sub\">Menu Area</legend>
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Only available on header alternative 2 & 6\">Menu Alignment</span></label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][main_menu_align]\" class=\"form-control\">
    \t<option value=\"menu-aligned-left\"";
        // line 102
        if (((isset($context["main_menu_align"]) ? $context["main_menu_align"] : null) == "menu-aligned-left")) {
            echo " selected=\"selected\"";
        }
        echo ">Left</option>
        <option value=\"menu-aligned-center\"";
        // line 103
        if (((isset($context["main_menu_align"]) ? $context["main_menu_align"] : null) == "menu-aligned-center")) {
            echo " selected=\"selected\"";
        }
        echo ">Center</option>
        <option value=\"menu-aligned-right\"";
        // line 104
        if (((isset($context["main_menu_align"]) ? $context["main_menu_align"] : null) == "menu-aligned-right")) {
            echo " selected=\"selected\"";
        }
        echo ">Right</option>
    </select>
</div>                   
</div>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><br /><span data-toggle=\"tooltip\" title=\"Enter values in pixels, eg. 100\">Menu Area Height</span></label>
    <div class=\"col-sm-10\">
    
    <div class=\"row\">
    <div class=\"col-sm-4\">
    <label>Default</label>
    <input class=\"form-control\" name=\"settings[basel][menu_height_normal]\" value=\"";
        // line 115
        echo (((isset($context["menu_height_normal"]) ? $context["menu_height_normal"] : null)) ? ((isset($context["menu_height_normal"]) ? $context["menu_height_normal"] : null)) : ("50"));
        echo "\" />
    </div>
    
    <div class=\"col-sm-4\">
    <label>Sticky Header</label>
    <input class=\"form-control\" name=\"settings[basel][menu_height_sticky]\" value=\"";
        // line 120
        echo (((isset($context["menu_height_sticky"]) ? $context["menu_height_sticky"] : null)) ? ((isset($context["menu_height_sticky"]) ? $context["menu_height_sticky"] : null)) : ("70"));
        echo "\" />
    </div>
        
    </div>
    </div>                   
</div>


<legend class=\"sub\">Header Promo Messages</legend>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Primary Promo Message</label>
    <div class=\"col-sm-10\">
    ";
        // line 133
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 134
            echo "    <div class=\"input-group\">
    <span class=\"input-group-addon\">
    <img src=\"language/";
            // line 136
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" /></span>
    <input class=\"form-control\" name=\"settings[basel][basel_promo][";
            // line 137
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["basel_promo"]) ? $context["basel_promo"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["basel_promo"]) ? $context["basel_promo"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" />
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 140
        echo "    </div>
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"If available in your header alternative\">Secondary Promo Message</span></label>
    <div class=\"col-sm-10\">
    ";
        // line 146
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 147
            echo "    <div class=\"input-group\">
    <span class=\"input-group-addon\">
    <img src=\"language/";
            // line 149
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" /></span>
    <input class=\"form-control\" name=\"settings[basel][basel_promo2][";
            // line 150
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["basel_promo2"]) ? $context["basel_promo2"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["basel_promo2"]) ? $context["basel_promo2"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" />
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 153
        echo "    </div>
</div>


<legend class=\"sub\">General</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enable to keep the header floating on your home page, with a transparent background\">Homepage Overlay Header</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_home_overlay_header]\" value=\"0\" ";
        // line 162
        if (((isset($context["basel_home_overlay_header"]) ? $context["basel_home_overlay_header"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_home_overlay_header]\" value=\"1\" ";
        // line 163
        if (((isset($context["basel_home_overlay_header"]) ? $context["basel_home_overlay_header"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Login / Register Status</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][header_login]\"  value=\"0\" ";
        // line 170
        if (((isset($context["header_login"]) ? $context["header_login"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][header_login]\"  value=\"1\" ";
        // line 171
        if (((isset($context["header_login"]) ? $context["header_login"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Header Search Status</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][header_search]\"  value=\"0\" ";
        // line 178
        if (((isset($context["header_search"]) ? $context["header_search"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][header_search]\"  value=\"1\" ";
        // line 179
        if (((isset($context["header_search"]) ? $context["header_search"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>
</div>

<legend>Menu Management</legend>
<div class=\"col-sm-offset-2\">
<div class=\"bs-callout bs-callout-info bs-callout-sm\">
<h4>How to manage menus?</h4>
<p>Please see the sketch at the header selection to see where menus are located.</p>
<p>As <b>Primary Menu</b> and <b>Secondary Menu</b> you can attach either the <i>Default menu from Opencart</i>, or a menu created using the <i>Basel Megamenu Module.</i></p>
<p>Default menu from Opencart = <i>All categories from Catalog > Categories where the \"Top\" checkbox is checked</i><br />
Mega Menu Module = <i>Menus created from the module Basel Megamenu under Extensions > Extensions -> Modules.</i></p>
<p><b>On mobile devices</b>, links from <i>Primary Menu</i>, <i>Secondary Menu</i> and <i>Static links</i> will be listed</p>
</div>
</div>
<legend class=\"sub\">Main Menus</legend>
<!-- PRIMARY MENU -->
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Select which menu to be used as the primary menu\">Primary Menu</span></label>
    <div class=\"col-sm-10\">
        <select name=\"settings[basel][primary_menu]\" class=\"form-control\">
        \t";
        // line 200
        if (((isset($context["primary_menu"]) ? $context["primary_menu"] : null) == "0")) {
            // line 201
            echo "            <option value=\"0\" selected=\"selected\">None</option>
            ";
        } else {
            // line 203
            echo "            <option value=\"0\">None</option>
            ";
        }
        // line 205
        echo "            ";
        if (((isset($context["primary_menu"]) ? $context["primary_menu"] : null) == "oc")) {
            // line 206
            echo "            <option value=\"oc\" selected=\"selected\">Default Opencart Category Menu</option>
            ";
        } else {
            // line 208
            echo "            <option value=\"oc\">Default Opencart Category Menu</option>
            ";
        }
        // line 210
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menu_modules"]) ? $context["menu_modules"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu_module"]) {
            // line 211
            echo "            ";
            if (((isset($context["primary_menu"]) ? $context["primary_menu"] : null) == $this->getAttribute($context["menu_module"], "module_id", array()))) {
                // line 212
                echo "            <option value=\"";
                echo $this->getAttribute($context["menu_module"], "module_id", array());
                echo "\" selected=\"selected\">Basel Megamenu: ";
                echo $this->getAttribute($context["menu_module"], "name", array());
                echo "</option>
            ";
            } else {
                // line 214
                echo "            <option value=\"";
                echo $this->getAttribute($context["menu_module"], "module_id", array());
                echo "\">Basel Megamenu: ";
                echo $this->getAttribute($context["menu_module"], "name", array());
                echo "</option>
            ";
            }
            // line 216
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 217
        echo "        </select>
    </div>                   
</div>

<!-- SECONDARY MENU -->
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Select which menu to be used as the secondary menu. (Only available on some header alternatives)\">Seconday Menu</span></label>
    <div class=\"col-sm-10\">
        <select name=\"settings[basel][secondary_menu]\" class=\"form-control\">
        \t";
        // line 226
        if (((isset($context["secondary_menu"]) ? $context["secondary_menu"] : null) == "0")) {
            // line 227
            echo "            <option value=\"0\" selected=\"selected\">None</option>
            ";
        } else {
            // line 229
            echo "            <option value=\"0\">None</option>
            ";
        }
        // line 231
        echo "            ";
        if (((isset($context["secondary_menu"]) ? $context["secondary_menu"] : null) == "oc")) {
            // line 232
            echo "            <option value=\"oc\" selected=\"selected\">Default Opencart Category Menu</option>
            ";
        } else {
            // line 234
            echo "            <option value=\"oc\">Default Opencart Category Menu</option>
            ";
        }
        // line 236
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menu_modules"]) ? $context["menu_modules"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu_module"]) {
            // line 237
            echo "            ";
            if (((isset($context["secondary_menu"]) ? $context["secondary_menu"] : null) == $this->getAttribute($context["menu_module"], "module_id", array()))) {
                // line 238
                echo "            <option value=\"";
                echo $this->getAttribute($context["menu_module"], "module_id", array());
                echo "\" selected=\"selected\">Basel Megamenu: ";
                echo $this->getAttribute($context["menu_module"], "name", array());
                echo "</option>
            ";
            } else {
                // line 240
                echo "            <option value=\"";
                echo $this->getAttribute($context["menu_module"], "module_id", array());
                echo "\">Basel Megamenu: ";
                echo $this->getAttribute($context["menu_module"], "name", array());
                echo "</option>
            ";
            }
            // line 242
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 243
        echo "        </select>
    </div>                   
</div>

<legend class=\"sub\">Header Static Links</legend>
<!-- STATIC LINKS -->
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"If disabled, the default links from opencart will be used\">Override Static Links</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][use_custom_links]\" class=\"links-select\" value=\"0\" ";
        // line 252
        if (((isset($context["use_custom_links"]) ? $context["use_custom_links"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][use_custom_links]\" class=\"links-select\" value=\"1\" ";
        // line 253
        if (((isset($context["use_custom_links"]) ? $context["use_custom_links"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"row\" id=\"custom_links_holder\"";
        // line 257
        if ((isset($context["use_custom_links"]) ? $context["use_custom_links"] : null)) {
            echo " style=\"display:block\"";
        } else {
            echo " style=\"display:none\"";
        }
        echo ">
<div class=\"col-sm-2\"></div>
<div class=\"col-sm-10\">
 <table id=\"links\" class=\"table table-clean\">
    <thead>
      <tr>
        <td width=\"48%\">Text</td>
        <td width=\"48%\"><span data-toggle=\"tooltip\" title=\"Include http:// when linking to external targets\">Link Target</span></td>
        <td width=\"4%\">Sort Order</td>
      </tr>
    </thead>
    <tbody>
     ";
        // line 269
        $context["link_row"] = 1;
        // line 270
        echo "     ";
        if (array_key_exists("link_row", $context)) {
            // line 271
            echo "\t  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["basel_links"]) ? $context["basel_links"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["basel_link"]) {
                // line 272
                echo "      <tr id=\"link-row";
                echo (isset($context["link_row"]) ? $context["link_row"] : null);
                echo "\">
        <td class=\"first\">
        ";
                // line 274
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                    // line 275
                    echo "        <div class=\"input-group\">
        <span class=\"input-group-addon\"><img src=\"language/";
                    // line 276
                    echo $this->getAttribute($context["language"], "code", array());
                    echo "/";
                    echo $this->getAttribute($context["language"], "code", array());
                    echo ".png\" title=\"";
                    echo $this->getAttribute($context["language"], "language_id", array());
                    echo "\" /></span>
        <input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_links][";
                    // line 277
                    echo (isset($context["link_row"]) ? $context["link_row"] : null);
                    echo "][text][";
                    echo $this->getAttribute($context["language"], "language_id", array());
                    echo "]\" value=\"";
                    echo (($this->getAttribute($this->getAttribute($context["basel_link"], "text", array()), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute($context["basel_link"], "text", array()), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
                    echo "\" size=\"40\" />
        </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 280
                echo "        </td>
        <td class=\"first\">
        <input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_links][";
                // line 282
                echo (isset($context["link_row"]) ? $context["link_row"] : null);
                echo "][target]\" value=\"";
                echo (($this->getAttribute($context["basel_link"], "target", array())) ? ($this->getAttribute($context["basel_link"], "target", array())) : (""));
                echo "\" />
        </td>
        <td class=\"first\">
        <input type=\"text\" class=\"form-control\" style=\"width:60px\" name=\"settings[basel][basel_links][";
                // line 285
                echo (isset($context["link_row"]) ? $context["link_row"] : null);
                echo "][sort]\" value=\"";
                echo (($this->getAttribute($context["basel_link"], "sort", array())) ? ($this->getAttribute($context["basel_link"], "sort", array())) : ("0"));
                echo "\" />
        </td>
        <td class=\"text-right\">
        <button type=\"button\" onclick=\"\$('#link-row";
                // line 288
                echo (isset($context["link_row"]) ? $context["link_row"] : null);
                echo "').remove();\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button>
        </td>
      </tr>
      ";
                // line 291
                $context["link_row"] = ((isset($context["link_row"]) ? $context["link_row"] : null) + 1);
                // line 292
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_link'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 293
            echo "    ";
        }
        // line 294
        echo "    </tbody>
    <tfoot>
      <tr>
        <td colspan=\"3\"></td>
        <td class=\"text-right\"><button type=\"button\" onclick=\"addLinkRow();\" data-toggle=\"tooltip\" title=\"";
        // line 298
        echo (isset($context["button_add"]) ? $context["button_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus-circle\"></i></button></td>
      </tr>
    </tfoot>
  </table>
 </div>
</div> <!-- row ends -->
          

<script type=\"text/javascript\"><!--
var link_row = ";
        // line 307
        echo (isset($context["link_row"]) ? $context["link_row"] : null);
        echo ";
function addLinkRow() {
\thtml  = '<tr id=\"link-row' + link_row + '\">';
\thtml += '<td class=\"first\">';
\t";
        // line 311
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 312
            echo "\thtml += '<div class=\"input-group\">';
\thtml += '<span class=\"input-group-addon\"><img src=\"language/";
            // line 313
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>';
\thtml += '<input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_links][' + link_row + '][text][";
            // line 314
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" />';
\thtml += '</div>';
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 317
        echo "\thtml += '</td>';
\thtml += '<td class=\"first\">';
\thtml += '<input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_links][' + link_row + '][target]\" />';
\thtml += '</td>';
\thtml += '<td class=\"first\">';
\thtml += '<input type=\"text\" class=\"form-control\" style=\"width:60px\" value=\"0\" name=\"settings[basel][basel_links][' + link_row + '][sort]\" />';
\thtml += '</td>';
\thtml += '<td class=\"text-right\"><button type=\"button\" onclick=\"\$(\\'#link-row' + link_row  + '\\').remove();\" data-toggle=\"tooltip\" title=\"";
        // line 324
        echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
        echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>';
\thtml += '</tr>';
\t\$('#links tbody').append(html);
\tlink_row++;
}
//--></script>";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  702 => 324,  693 => 317,  684 => 314,  676 => 313,  673 => 312,  669 => 311,  662 => 307,  650 => 298,  644 => 294,  641 => 293,  635 => 292,  633 => 291,  625 => 288,  617 => 285,  609 => 282,  605 => 280,  592 => 277,  584 => 276,  581 => 275,  577 => 274,  571 => 272,  566 => 271,  563 => 270,  561 => 269,  542 => 257,  533 => 253,  527 => 252,  516 => 243,  510 => 242,  502 => 240,  494 => 238,  491 => 237,  486 => 236,  482 => 234,  478 => 232,  475 => 231,  471 => 229,  467 => 227,  465 => 226,  454 => 217,  448 => 216,  440 => 214,  432 => 212,  429 => 211,  424 => 210,  420 => 208,  416 => 206,  413 => 205,  409 => 203,  405 => 201,  403 => 200,  377 => 179,  371 => 178,  359 => 171,  353 => 170,  341 => 163,  335 => 162,  324 => 153,  313 => 150,  305 => 149,  301 => 147,  297 => 146,  289 => 140,  278 => 137,  270 => 136,  266 => 134,  262 => 133,  246 => 120,  238 => 115,  222 => 104,  216 => 103,  210 => 102,  198 => 93,  185 => 85,  179 => 84,  166 => 74,  158 => 69,  150 => 64,  134 => 53,  128 => 52,  116 => 43,  103 => 35,  97 => 34,  84 => 26,  78 => 25,  65 => 15,  57 => 12,  51 => 11,  45 => 10,  39 => 9,  33 => 8,  27 => 7,  19 => 1,);
    }
}
/* <legend>Header Styling</legend>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Header Alternative</label>*/
/* <div class="col-sm-10">*/
/*     <select id="header-select" name="settings[basel][basel_header]" class="form-control">*/
/*         <option value="header1"{% if basel_header == 'header1' %} selected="selected"{% endif %}>Header 1</option>*/
/*         <option value="header2"{% if basel_header == 'header2' %} selected="selected"{% endif %}>Header 2</option>*/
/*         <option value="header3"{% if basel_header == 'header3' %} selected="selected"{% endif %}>Header 3</option>*/
/*         <option value="header4"{% if basel_header == 'header4' %} selected="selected"{% endif %}>Header 4</option>*/
/*         <option value="header5"{% if basel_header == 'header5' %} selected="selected"{% endif %}>Header 5</option>*/
/*         <option value="header6"{% if basel_header == 'header6' %} selected="selected"{% endif %}>Header 6</option>*/
/*     </select>*/
/*     <div id="header-preview" class="preview-holder">*/
/*     <img src="view/javascript/basel/img/theme-panel/headers/{{ basel_header }}.png" />*/
/*     </div>*/
/* </div>*/
/* </div>*/
/* */
/* <legend class="sub">Top Line</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Top Line Status</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][top_line_style]" value="0" {% if top_line_style == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][top_line_style]" value="1" {% if top_line_style == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Top Line Width</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][top_line_width]" class="form-control">*/
/*         <option value="full-width"{% if top_line_width == 'full-width' %} selected="selected"{% endif %}>Full Width</option>*/
/*         <option value="boxed"{% if top_line_width == 'boxed' %} selected="selected"{% endif %}>Boxed</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enter value in pixels, eg. 41">Top Line Height</span></label>*/
/* <div class="col-sm-10">*/
/*     <input class="form-control" name="settings[basel][top_line_height]" value="{{ top_line_height ? top_line_height : '41' }}" />*/
/* </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Main Header Area</legend>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Main Area Width</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][main_header_width]" class="form-control">*/
/*         <option value="full-width"{% if main_header_width == 'full-width' %} selected="selected"{% endif %}>Full Width</option>*/
/*         <option value="boxed"{% if main_header_width == 'boxed' %} selected="selected"{% endif %}>Boxed</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><br /><span data-toggle="tooltip" title="Enter values in pixels, eg. 100">Main Header Area Height</span></label>*/
/*     <div class="col-sm-10">*/
/*     */
/*     <div class="row">*/
/*     <div class="col-sm-4">*/
/*     <label>Desktop View</label>*/
/*     <input class="form-control" name="settings[basel][main_header_height]" value="{{ main_header_height ? main_header_height : '104' }}" />*/
/*     </div>*/
/*     */
/*     <div class="col-sm-4">*/
/*     <label>Mobile View</label>*/
/*     <input class="form-control" name="settings[basel][main_header_height_mobile]" value="{{ main_header_height_mobile ? main_header_height_mobile : '70' }}" />*/
/*     </div>*/
/*     */
/*     <div class="col-sm-4">*/
/*     <label>Sticky Header</label>*/
/*     <input class="form-control" name="settings[basel][main_header_height_sticky]" value="{{ main_header_height_sticky ? main_header_height_sticky : '70' }}" />*/
/*     </div>*/
/*         */
/*     </div>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Keep the header fixed on top of the page when scrolling">Sticky Header</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_sticky_header]" value="0" {% if basel_sticky_header == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_sticky_header]" value="1" {% if basel_sticky_header == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Logo</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enter value in pixels, eg. 100">Logo Max Width</span></label>*/
/*     <div class="col-sm-10">*/
/*     <input class="form-control" name="settings[basel][logo_maxwidth]" value="{{ logo_maxwidth ? logo_maxwidth : '250' }}" />        */
/*     </div>*/
/* </div>*/
/* */
/* <legend class="sub">Menu Area</legend>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Only available on header alternative 2 & 6">Menu Alignment</span></label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][main_menu_align]" class="form-control">*/
/*     	<option value="menu-aligned-left"{% if main_menu_align == 'menu-aligned-left' %} selected="selected"{% endif %}>Left</option>*/
/*         <option value="menu-aligned-center"{% if main_menu_align == 'menu-aligned-center' %} selected="selected"{% endif %}>Center</option>*/
/*         <option value="menu-aligned-right"{% if main_menu_align == 'menu-aligned-right' %} selected="selected"{% endif %}>Right</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><br /><span data-toggle="tooltip" title="Enter values in pixels, eg. 100">Menu Area Height</span></label>*/
/*     <div class="col-sm-10">*/
/*     */
/*     <div class="row">*/
/*     <div class="col-sm-4">*/
/*     <label>Default</label>*/
/*     <input class="form-control" name="settings[basel][menu_height_normal]" value="{{ menu_height_normal ? menu_height_normal : '50' }}" />*/
/*     </div>*/
/*     */
/*     <div class="col-sm-4">*/
/*     <label>Sticky Header</label>*/
/*     <input class="form-control" name="settings[basel][menu_height_sticky]" value="{{ menu_height_sticky ? menu_height_sticky : '70' }}" />*/
/*     </div>*/
/*         */
/*     </div>*/
/*     </div>                   */
/* </div>*/
/* */
/* */
/* <legend class="sub">Header Promo Messages</legend>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Primary Promo Message</label>*/
/*     <div class="col-sm-10">*/
/*     {% for language in languages %}*/
/*     <div class="input-group">*/
/*     <span class="input-group-addon">*/
/*     <img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.language_id }}" /></span>*/
/*     <input class="form-control" name="settings[basel][basel_promo][{{ language.language_id }}]" value="{{ basel_promo[language.language_id] ? basel_promo[language.language_id] }}" />*/
/*     </div>*/
/*     {% endfor %}*/
/*     </div>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="If available in your header alternative">Secondary Promo Message</span></label>*/
/*     <div class="col-sm-10">*/
/*     {% for language in languages %}*/
/*     <div class="input-group">*/
/*     <span class="input-group-addon">*/
/*     <img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.language_id }}" /></span>*/
/*     <input class="form-control" name="settings[basel][basel_promo2][{{ language.language_id }}]" value="{{ basel_promo2[language.language_id] ? basel_promo2[language.language_id] }}" />*/
/*     </div>*/
/*     {% endfor %}*/
/*     </div>*/
/* </div>*/
/* */
/* */
/* <legend class="sub">General</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enable to keep the header floating on your home page, with a transparent background">Homepage Overlay Header</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_home_overlay_header]" value="0" {% if basel_home_overlay_header == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_home_overlay_header]" value="1" {% if basel_home_overlay_header == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Login / Register Status</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][header_login]"  value="0" {% if header_login == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][header_login]"  value="1" {% if header_login == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Header Search Status</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][header_search]"  value="0" {% if header_search == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][header_search]"  value="1" {% if header_search == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>*/
/* </div>*/
/* */
/* <legend>Menu Management</legend>*/
/* <div class="col-sm-offset-2">*/
/* <div class="bs-callout bs-callout-info bs-callout-sm">*/
/* <h4>How to manage menus?</h4>*/
/* <p>Please see the sketch at the header selection to see where menus are located.</p>*/
/* <p>As <b>Primary Menu</b> and <b>Secondary Menu</b> you can attach either the <i>Default menu from Opencart</i>, or a menu created using the <i>Basel Megamenu Module.</i></p>*/
/* <p>Default menu from Opencart = <i>All categories from Catalog > Categories where the "Top" checkbox is checked</i><br />*/
/* Mega Menu Module = <i>Menus created from the module Basel Megamenu under Extensions > Extensions -> Modules.</i></p>*/
/* <p><b>On mobile devices</b>, links from <i>Primary Menu</i>, <i>Secondary Menu</i> and <i>Static links</i> will be listed</p>*/
/* </div>*/
/* </div>*/
/* <legend class="sub">Main Menus</legend>*/
/* <!-- PRIMARY MENU -->*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Select which menu to be used as the primary menu">Primary Menu</span></label>*/
/*     <div class="col-sm-10">*/
/*         <select name="settings[basel][primary_menu]" class="form-control">*/
/*         	{% if primary_menu == '0' %}*/
/*             <option value="0" selected="selected">None</option>*/
/*             {% else %}*/
/*             <option value="0">None</option>*/
/*             {% endif %}*/
/*             {% if primary_menu == 'oc' %}*/
/*             <option value="oc" selected="selected">Default Opencart Category Menu</option>*/
/*             {% else %}*/
/*             <option value="oc">Default Opencart Category Menu</option>*/
/*             {% endif %}*/
/*         {% for menu_module in menu_modules %}*/
/*             {% if primary_menu == menu_module.module_id %}*/
/*             <option value="{{ menu_module.module_id }}" selected="selected">Basel Megamenu: {{ menu_module.name }}</option>*/
/*             {% else %}*/
/*             <option value="{{ menu_module.module_id }}">Basel Megamenu: {{ menu_module.name }}</option>*/
/*             {% endif %}*/
/*         {% endfor %}*/
/*         </select>*/
/*     </div>                   */
/* </div>*/
/* */
/* <!-- SECONDARY MENU -->*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Select which menu to be used as the secondary menu. (Only available on some header alternatives)">Seconday Menu</span></label>*/
/*     <div class="col-sm-10">*/
/*         <select name="settings[basel][secondary_menu]" class="form-control">*/
/*         	{% if secondary_menu == '0' %}*/
/*             <option value="0" selected="selected">None</option>*/
/*             {% else %}*/
/*             <option value="0">None</option>*/
/*             {% endif %}*/
/*             {% if secondary_menu == 'oc' %}*/
/*             <option value="oc" selected="selected">Default Opencart Category Menu</option>*/
/*             {% else %}*/
/*             <option value="oc">Default Opencart Category Menu</option>*/
/*             {% endif %}*/
/*         {% for menu_module in menu_modules %}*/
/*             {% if secondary_menu == menu_module.module_id %}*/
/*             <option value="{{ menu_module.module_id }}" selected="selected">Basel Megamenu: {{ menu_module.name }}</option>*/
/*             {% else %}*/
/*             <option value="{{ menu_module.module_id }}">Basel Megamenu: {{ menu_module.name }}</option>*/
/*             {% endif %}*/
/*         {% endfor %}*/
/*         </select>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Header Static Links</legend>*/
/* <!-- STATIC LINKS -->*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="If disabled, the default links from opencart will be used">Override Static Links</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][use_custom_links]" class="links-select" value="0" {% if use_custom_links == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][use_custom_links]" class="links-select" value="1" {% if use_custom_links == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="row" id="custom_links_holder"{% if use_custom_links %} style="display:block"{% else %} style="display:none"{% endif %}>*/
/* <div class="col-sm-2"></div>*/
/* <div class="col-sm-10">*/
/*  <table id="links" class="table table-clean">*/
/*     <thead>*/
/*       <tr>*/
/*         <td width="48%">Text</td>*/
/*         <td width="48%"><span data-toggle="tooltip" title="Include http:// when linking to external targets">Link Target</span></td>*/
/*         <td width="4%">Sort Order</td>*/
/*       </tr>*/
/*     </thead>*/
/*     <tbody>*/
/*      {% set link_row = 1 %}*/
/*      {% if link_row is defined %}*/
/* 	  {% for basel_link in basel_links %}*/
/*       <tr id="link-row{{ link_row }}">*/
/*         <td class="first">*/
/*         {% for language in languages %}*/
/*         <div class="input-group">*/
/*         <span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.language_id }}" /></span>*/
/*         <input type="text" class="form-control" name="settings[basel][basel_links][{{ link_row }}][text][{{ language.language_id }}]" value="{{ basel_link.text[language.language_id] ? basel_link.text[language.language_id] }}" size="40" />*/
/*         </div>*/
/*         {% endfor %}*/
/*         </td>*/
/*         <td class="first">*/
/*         <input type="text" class="form-control" name="settings[basel][basel_links][{{ link_row }}][target]" value="{{ basel_link.target ? basel_link.target }}" />*/
/*         </td>*/
/*         <td class="first">*/
/*         <input type="text" class="form-control" style="width:60px" name="settings[basel][basel_links][{{ link_row }}][sort]" value="{{ basel_link.sort ? basel_link.sort : '0' }}" />*/
/*         </td>*/
/*         <td class="text-right">*/
/*         <button type="button" onclick="$('#link-row{{ link_row }}').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>*/
/*         </td>*/
/*       </tr>*/
/*       {% set link_row = link_row + 1 %}*/
/*     {% endfor %}*/
/*     {% endif %}*/
/*     </tbody>*/
/*     <tfoot>*/
/*       <tr>*/
/*         <td colspan="3"></td>*/
/*         <td class="text-right"><button type="button" onclick="addLinkRow();" data-toggle="tooltip" title="{{ button_add }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>*/
/*       </tr>*/
/*     </tfoot>*/
/*   </table>*/
/*  </div>*/
/* </div> <!-- row ends -->*/
/*           */
/* */
/* <script type="text/javascript"><!--*/
/* var link_row = {{ link_row }};*/
/* function addLinkRow() {*/
/* 	html  = '<tr id="link-row' + link_row + '">';*/
/* 	html += '<td class="first">';*/
/* 	{% for language in languages %}*/
/* 	html += '<div class="input-group">';*/
/* 	html += '<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>';*/
/* 	html += '<input type="text" class="form-control" name="settings[basel][basel_links][' + link_row + '][text][{{ language.language_id }}]" />';*/
/* 	html += '</div>';*/
/* 	{% endfor %}*/
/* 	html += '</td>';*/
/* 	html += '<td class="first">';*/
/* 	html += '<input type="text" class="form-control" name="settings[basel][basel_links][' + link_row + '][target]" />';*/
/* 	html += '</td>';*/
/* 	html += '<td class="first">';*/
/* 	html += '<input type="text" class="form-control" style="width:60px" value="0" name="settings[basel][basel_links][' + link_row + '][sort]" />';*/
/* 	html += '</td>';*/
/* 	html += '<td class="text-right"><button type="button" onclick="$(\'#link-row' + link_row  + '\').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';*/
/* 	html += '</tr>';*/
/* 	$('#links tbody').append(html);*/
/* 	link_row++;*/
/* }*/
/* //--></script>*/
