<?php

/* extension/basel/panel_tabs/styles.twig */
class __TwigTemplate_3c21a4888d4dd5c1dcee9b41fda591ebb839347ae689da67322770c5aaf83072 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Styles & Colors</legend>

<legend class=\"sub\">General</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Shopping cart icon</label>
    <div class=\"col-sm-10 image-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_cart_icon]\" value=\"global-cart-bag\" ";
        // line 7
        if (((isset($context["basel_cart_icon"]) ? $context["basel_cart_icon"] : null) == "global-cart-bag")) {
            echo " checked=\"checked\"";
        }
        echo " /><span><i class=\"icon-bag\"></i></span></label>
    
    <label><input type=\"radio\" name=\"settings[basel][basel_cart_icon]\" value=\"global-cart-basket\" ";
        // line 9
        if (((isset($context["basel_cart_icon"]) ? $context["basel_cart_icon"] : null) == "global-cart-basket")) {
            echo " checked=\"checked\"";
        }
        echo " /><span><i class=\"icon-basket\"></i></span></label>
    
    <label><input type=\"radio\" name=\"settings[basel][basel_cart_icon]\" value=\"global-cart-handbag\" ";
        // line 11
        if (((isset($context["basel_cart_icon"]) ? $context["basel_cart_icon"] : null) == "global-cart-handbag")) {
            echo " checked=\"checked\"";
        }
        echo " /><span><i class=\"icon-handbag\"></i></span></label>
    
    <label><input type=\"radio\" name=\"settings[basel][basel_cart_icon]\" value=\"global-cart-briefcase\" ";
        // line 13
        if (((isset($context["basel_cart_icon"]) ? $context["basel_cart_icon"] : null) == "global-cart-briefcase")) {
            echo " checked=\"checked\"";
        }
        echo " /><span><i class=\"icon-briefcase\"></i></span></label>
    
    <label><input type=\"radio\" name=\"settings[basel][basel_cart_icon]\" value=\"global-cart-shoppingbag\" ";
        // line 15
        if (((isset($context["basel_cart_icon"]) ? $context["basel_cart_icon"] : null) == "global-cart-shoppingbag")) {
            echo " checked=\"checked\"";
        }
        echo " /><span><i class=\"icon-shopping-bag\"></i></span></label>
    
    <label><input type=\"radio\" name=\"settings[basel][basel_cart_icon]\" value=\"global-cart-shoppingbasket\" ";
        // line 17
        if (((isset($context["basel_cart_icon"]) ? $context["basel_cart_icon"] : null) == "global-cart-shoppingbasket")) {
            echo " checked=\"checked\"";
        }
        echo " /><span><i class=\"icon-shopping-basket\"></i></span></label>
    </div>                   
</div>

<legend class=\"sub\">Layout</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Layout Style</label>
    <div class=\"col-sm-10 toggle-btn both-blue\">
    <label><input type=\"radio\" name=\"settings[basel][basel_main_layout]\" value=\"0\" ";
        // line 25
        if (((isset($context["basel_main_layout"]) ? $context["basel_main_layout"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Full width</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_main_layout]\" value=\"1\" ";
        // line 26
        if (((isset($context["basel_main_layout"]) ? $context["basel_main_layout"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Boxed width</span></label>
    </div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Content width</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_content_width]\" class=\"form-control\">
        <option value=\"narrow_container\"";
        // line 34
        if (((isset($context["basel_content_width"]) ? $context["basel_content_width"] : null) == "narrow_container")) {
            echo " selected=\"selected\"";
        }
        echo ">Narrow (1060px)</option>
        <option value=\"\"";
        // line 35
        if (((isset($context["basel_content_width"]) ? $context["basel_content_width"] : null) == "")) {
            echo " selected=\"selected\"";
        }
        echo ">Normal (1170px)</option>
        <option value=\"wide_container\"";
        // line 36
        if (((isset($context["basel_content_width"]) ? $context["basel_content_width"] : null) == "wide_container")) {
            echo " selected=\"selected\"";
        }
        echo ">Wide (1280px)</option>
    </select>
</div>                   
</div>

<legend class=\"sub\">Sticky Columns</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Sticky columns stays within the viewport when scrolling down\">Sticky Columns</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_sticky_columns]\" value=\"0\" ";
        // line 45
        if (((isset($context["basel_sticky_columns"]) ? $context["basel_sticky_columns"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_sticky_columns]\" value=\"1\" ";
        // line 46
        if (((isset($context["basel_sticky_columns"]) ? $context["basel_sticky_columns"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Use offset if sticky header is enabled to avoid overlapping columns/header. Enter a value in pixels, for example 100\">Sticky Offset Top</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <input class=\"form-control\" style=\"width:120px\" name=\"settings[basel][basel_sticky_columns_offset]\" value=\"";
        // line 53
        echo (((isset($context["basel_sticky_columns_offset"]) ? $context["basel_sticky_columns_offset"] : null)) ? ((isset($context["basel_sticky_columns_offset"]) ? $context["basel_sticky_columns_offset"] : null)) : ("100"));
        echo "\" />
    </div>                   
</div>

<legend class=\"sub\">Widget Titles</legend>
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Select heading style on modules\">Widget heading style</span></label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_widget_title_style]\" class=\"form-control\">
        <option value=\"0\"";
        // line 62
        if (((isset($context["basel_widget_title_style"]) ? $context["basel_widget_title_style"] : null) == "0")) {
            echo " selected=\"selected\"";
        }
        echo ">Style 1 - (Cross Separator)</option>
        <option value=\"2\"";
        // line 63
        if (((isset($context["basel_widget_title_style"]) ? $context["basel_widget_title_style"] : null) == "2")) {
            echo " selected=\"selected\"";
        }
        echo ">Style 2 - (Line Separator)</option>
        <option value=\"3\"";
        // line 64
        if (((isset($context["basel_widget_title_style"]) ? $context["basel_widget_title_style"] : null) == "3")) {
            echo " selected=\"selected\"";
        }
        echo ">Style 3 - (Bordered Title, No Separator)</option>
    </select>
</div>                   
</div>

<legend class=\"sub\">Product Listing Style</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Select how to show products in product listings, like category pages, related products etc.\">Product listing style</span></label>
    <div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_list_style]\" class=\"form-control\">
        <option value=\"1\"";
        // line 74
        if (((isset($context["basel_list_style"]) ? $context["basel_list_style"] : null) == "1")) {
            echo " selected=\"selected\"";
        }
        echo ">Style 1 - Default style</option>
        <option value=\"2\"";
        // line 75
        if (((isset($context["basel_list_style"]) ? $context["basel_list_style"] : null) == "2")) {
            echo " selected=\"selected\"";
        }
        echo ">Style 2 - Action buttons slide up on hover</option>
        <option value=\"3\"";
        // line 76
        if (((isset($context["basel_list_style"]) ? $context["basel_list_style"] : null) == "3")) {
            echo " selected=\"selected\"";
        }
        echo ">Style 3 - Dark image overlay on hover</option>
        <option value=\"4\"";
        // line 77
        if (((isset($context["basel_list_style"]) ? $context["basel_list_style"] : null) == "4")) {
            echo " selected=\"selected\"";
        }
        echo ">Style 4 - Dark image overlay on hover alt 2</option>
        <option value=\"1 names-c\"";
        // line 78
        if (((isset($context["basel_list_style"]) ? $context["basel_list_style"] : null) == "1 names-c")) {
            echo " selected=\"selected\"";
        }
        echo ">Style 5 - Center aligned</option>
        <option value=\"6\"";
        // line 79
        if (((isset($context["basel_list_style"]) ? $context["basel_list_style"] : null) == "6")) {
            echo " selected=\"selected\"";
        }
        echo ">Style 6 - Center aligned with button</option>
    </select>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Show the first additional image when hovering a product\">Swap image on hover</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_thumb_swap]\" value=\"0\" ";
        // line 87
        if (((isset($context["basel_thumb_swap"]) ? $context["basel_thumb_swap"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_thumb_swap]\" value=\"1\" ";
        // line 88
        if (((isset($context["basel_thumb_swap"]) ? $context["basel_thumb_swap"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Keep the product names in listings within one line (using only CSS)\">Cut product names</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_cut_names]\" value=\"0\" ";
        // line 95
        if (((isset($context["basel_cut_names"]) ? $context["basel_cut_names"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_cut_names]\" value=\"1\" ";
        // line 96
        if (((isset($context["basel_cut_names"]) ? $context["basel_cut_names"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"On smaller mobile devices, select to view 1 or 2 items per row\">Minimum items per row</span></label>
    <div class=\"col-sm-10 toggle-btn both-blue\">
    <label><input type=\"radio\" name=\"settings[basel][items_mobile_fw]\" value=\"1\" ";
        // line 103
        if (((isset($context["items_mobile_fw"]) ? $context["items_mobile_fw"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>1 item</span></label>
    <label><input type=\"radio\" name=\"settings[basel][items_mobile_fw]\" value=\"0\" ";
        // line 104
        if (((isset($context["items_mobile_fw"]) ? $context["items_mobile_fw"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>2 items</span></label>
    </div>                   
</div>

<legend>Custom Color Scheme</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Override default colors</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_design_status]\" class=\"design-select\" value=\"0\" ";
        // line 112
        if (((isset($context["basel_design_status"]) ? $context["basel_design_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_design_status]\" class=\"design-select\" value=\"1\" ";
        // line 113
        if (((isset($context["basel_design_status"]) ? $context["basel_design_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div id=\"custom_design_holder\"";
        // line 117
        if ((isset($context["basel_design_status"]) ? $context["basel_design_status"] : null)) {
            echo " style=\"display:block\"";
        } else {
            echo " style=\"display:none\"";
        }
        echo ">

<legend class=\"third\">Body Background (when using boxed layout)</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 125
        echo (((isset($context["basel_body_bg_color"]) ? $context["basel_body_bg_color"] : null)) ? ((isset($context["basel_body_bg_color"]) ? $context["basel_body_bg_color"] : null)) : ("#ececec"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_body_bg_color]\" value=\"";
        // line 126
        echo (((isset($context["basel_body_bg_color"]) ? $context["basel_body_bg_color"] : null)) ? ((isset($context["basel_body_bg_color"]) ? $context["basel_body_bg_color"] : null)) : ("#ececec"));
        echo "\" />
    </div> 
    </div>                  
</div>


<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Background Image</label>
    <div class=\"col-sm-10\">
    <div class=\"row\">
        <div class=\"col-sm-2\">
        <a href=\"\" id=\"thumb-bc-img\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 137
        echo (isset($context["body_thumb"]) ? $context["body_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
    <input type=\"hidden\" name=\"settings[basel][basel_body_bg_img]\" value=\"";
        // line 138
        echo (isset($context["basel_body_bg_img"]) ? $context["basel_body_bg_img"] : null);
        echo "\" id=\"input-body-img\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Background Position</label>
        <select name=\"settings[basel][basel_body_bg_img_pos]\" class=\"form-control\">
        <option value=\"top left\"";
        // line 143
        if (((isset($context["basel_body_bg_img_pos"]) ? $context["basel_body_bg_img_pos"] : null) == "top left")) {
            echo " selected=\"selected\"";
        }
        echo ">top left</option>
        <option value=\"top center\"";
        // line 144
        if (((isset($context["basel_body_bg_img_pos"]) ? $context["basel_body_bg_img_pos"] : null) == "top center")) {
            echo " selected=\"selected\"";
        }
        echo ">top center</option>
        <option value=\"top right\"";
        // line 145
        if (((isset($context["basel_body_bg_img_pos"]) ? $context["basel_body_bg_img_pos"] : null) == "top right")) {
            echo " selected=\"selected\"";
        }
        echo ">top right</option>
        <option value=\"center left\"";
        // line 146
        if (((isset($context["basel_body_bg_img_pos"]) ? $context["basel_body_bg_img_pos"] : null) == "center left")) {
            echo " selected=\"selected\"";
        }
        echo ">middle left</option>
        <option value=\"center center\"";
        // line 147
        if (((isset($context["basel_body_bg_img_pos"]) ? $context["basel_body_bg_img_pos"] : null) == "center center")) {
            echo " selected=\"selected\"";
        }
        echo ">middle center</option>
        <option value=\"center right\"";
        // line 148
        if (((isset($context["basel_body_bg_img_pos"]) ? $context["basel_body_bg_img_pos"] : null) == "center right")) {
            echo " selected=\"selected\"";
        }
        echo ">middle right</option>
        <option value=\"bottom left\"";
        // line 149
        if (((isset($context["basel_body_bg_img_pos"]) ? $context["basel_body_bg_img_pos"] : null) == "bottom left")) {
            echo " selected=\"selected\"";
        }
        echo ">bottom left</option>
        <option value=\"bottom center\"";
        // line 150
        if (((isset($context["basel_body_bg_img_pos"]) ? $context["basel_body_bg_img_pos"] : null) == "bottom center")) {
            echo " selected=\"selected\"";
        }
        echo ">bottom center</option>
        <option value=\"bottom right\"";
        // line 151
        if (((isset($context["basel_body_bg_img_pos"]) ? $context["basel_body_bg_img_pos"] : null) == "bottom right")) {
            echo " selected=\"selected\"";
        }
        echo ">bottom right</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Background Size</label>
        <select name=\"settings[basel][basel_body_bg_img_size]\" class=\"form-control\">
        <option value=\"auto\"";
        // line 157
        if (((isset($context["basel_body_bg_img_size"]) ? $context["basel_body_bg_img_size"] : null) == "auto")) {
            echo " selected=\"selected\"";
        }
        echo ">auto</option>
        <option value=\"contain\"";
        // line 158
        if (((isset($context["basel_body_bg_img_size"]) ? $context["basel_body_bg_img_size"] : null) == "contain")) {
            echo " selected=\"selected\"";
        }
        echo ">contain</option>
        <option value=\"cover\"";
        // line 159
        if (((isset($context["basel_body_bg_img_size"]) ? $context["basel_body_bg_img_size"] : null) == "cover")) {
            echo " selected=\"selected\"";
        }
        echo ">cover</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Background Size</label>
        <select name=\"settings[basel][basel_body_bg_img_repeat]\" class=\"form-control\">
        <option value=\"no-repeat\"";
        // line 165
        if (((isset($context["basel_body_bg_img_repeat"]) ? $context["basel_body_bg_img_repeat"] : null) == "no-repeat")) {
            echo " selected=\"selected\"";
        }
        echo ">no-repeat</option>
        <option value=\"repeat-x\"";
        // line 166
        if (((isset($context["basel_body_bg_img_repeat"]) ? $context["basel_body_bg_img_repeat"] : null) == "repeat-x")) {
            echo " selected=\"selected\"";
        }
        echo ">repeat-x (-)</option>
        <option value=\"repeat-y\"";
        // line 167
        if (((isset($context["basel_body_bg_img_repeat"]) ? $context["basel_body_bg_img_repeat"] : null) == "repeat-y")) {
            echo " selected=\"selected\"";
        }
        echo ">repeat-y (|)</option>
        <option value=\"repeat\"";
        // line 168
        if (((isset($context["basel_body_bg_img_repeat"]) ? $context["basel_body_bg_img_repeat"] : null) == "repeat")) {
            echo " selected=\"selected\"";
        }
        echo ">repeat</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Background Attachment</label>
        <select name=\"settings[basel][basel_body_bg_img_att]\" class=\"form-control\">
        <option value=\"scroll\"";
        // line 174
        if (((isset($context["basel_body_bg_img_att"]) ? $context["basel_body_bg_img_att"] : null) == "scroll")) {
            echo " selected=\"selected\"";
        }
        echo ">scroll</option>
        <option value=\"local\"";
        // line 175
        if (((isset($context["basel_body_bg_img_att"]) ? $context["basel_body_bg_img_att"] : null) == "local")) {
            echo " selected=\"selected\"";
        }
        echo ">local</option>
        <option value=\"fixed\"";
        // line 176
        if (((isset($context["basel_body_bg_img_att"]) ? $context["basel_body_bg_img_att"] : null) == "fixed")) {
            echo " selected=\"selected\"";
        }
        echo ">fixed</option>
        </select>
        </div>        
            
    </div>
    </div>                   
</div>


<legend class=\"third\">Top Line Promo Message</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 190
        echo (((isset($context["basel_top_note_bg"]) ? $context["basel_top_note_bg"] : null)) ? ((isset($context["basel_top_note_bg"]) ? $context["basel_top_note_bg"] : null)) : ("#000000"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_top_note_bg]\" value=\"";
        // line 191
        echo (((isset($context["basel_top_note_bg"]) ? $context["basel_top_note_bg"] : null)) ? ((isset($context["basel_top_note_bg"]) ? $context["basel_top_note_bg"] : null)) : ("#000000"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 200
        echo (((isset($context["basel_top_note_color"]) ? $context["basel_top_note_color"] : null)) ? ((isset($context["basel_top_note_color"]) ? $context["basel_top_note_color"] : null)) : ("#eeeeee"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_top_note_color]\" value=\"";
        // line 201
        echo (((isset($context["basel_top_note_color"]) ? $context["basel_top_note_color"] : null)) ? ((isset($context["basel_top_note_color"]) ? $context["basel_top_note_color"] : null)) : ("#eeeeee"));
        echo "\" />
    </div> 
    </div>                 
</div>

<legend class=\"third\">Top Line</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 211
        echo (((isset($context["basel_top_line_bg"]) ? $context["basel_top_line_bg"] : null)) ? ((isset($context["basel_top_line_bg"]) ? $context["basel_top_line_bg"] : null)) : ("#1daaa3"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_top_line_bg]\" value=\"";
        // line 212
        echo (((isset($context["basel_top_line_bg"]) ? $context["basel_top_line_bg"] : null)) ? ((isset($context["basel_top_line_bg"]) ? $context["basel_top_line_bg"] : null)) : ("#1daaa3"));
        echo "\" />
    </div> 
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 221
        echo (((isset($context["basel_top_line_color"]) ? $context["basel_top_line_color"] : null)) ? ((isset($context["basel_top_line_color"]) ? $context["basel_top_line_color"] : null)) : ("#ffffff"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_top_line_color]\" value=\"";
        // line 222
        echo (((isset($context["basel_top_line_color"]) ? $context["basel_top_line_color"] : null)) ? ((isset($context["basel_top_line_color"]) ? $context["basel_top_line_color"] : null)) : ("#ffffff"));
        echo "\" />
    </div> 
    </div>                   
</div>

<legend class=\"third\">Header Area</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 232
        echo (((isset($context["basel_header_bg"]) ? $context["basel_header_bg"] : null)) ? ((isset($context["basel_header_bg"]) ? $context["basel_header_bg"] : null)) : ("#ffffff"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_header_bg]\" value=\"";
        // line 233
        echo (((isset($context["basel_header_bg"]) ? $context["basel_header_bg"] : null)) ? ((isset($context["basel_header_bg"]) ? $context["basel_header_bg"] : null)) : ("#ffffff"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 242
        echo (((isset($context["basel_header_color"]) ? $context["basel_header_color"] : null)) ? ((isset($context["basel_header_color"]) ? $context["basel_header_color"] : null)) : ("#000000"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_header_color]\" value=\"";
        // line 243
        echo (((isset($context["basel_header_color"]) ? $context["basel_header_color"] : null)) ? ((isset($context["basel_header_color"]) ? $context["basel_header_color"] : null)) : ("#000000"));
        echo "\" />
    </div> 
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Color on details in header, for example product counters\">Header accent color</span></label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 252
        echo (((isset($context["basel_header_accent"]) ? $context["basel_header_accent"] : null)) ? ((isset($context["basel_header_accent"]) ? $context["basel_header_accent"] : null)) : ("#1daaa3"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_header_accent]\" value=\"";
        // line 253
        echo (((isset($context["basel_header_accent"]) ? $context["basel_header_accent"] : null)) ? ((isset($context["basel_header_accent"]) ? $context["basel_header_accent"] : null)) : ("#1daaa3"));
        echo "\" />
    </div> 
    </div>                  
</div>

<legend class=\"third\">Menu</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 263
        echo (((isset($context["basel_header_menu_bg"]) ? $context["basel_header_menu_bg"] : null)) ? ((isset($context["basel_header_menu_bg"]) ? $context["basel_header_menu_bg"] : null)) : ("#111111"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_header_menu_bg]\" value=\"";
        // line 264
        echo (((isset($context["basel_header_menu_bg"]) ? $context["basel_header_menu_bg"] : null)) ? ((isset($context["basel_header_menu_bg"]) ? $context["basel_header_menu_bg"] : null)) : ("#111111"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 273
        echo (((isset($context["basel_header_menu_color"]) ? $context["basel_header_menu_color"] : null)) ? ((isset($context["basel_header_menu_color"]) ? $context["basel_header_menu_color"] : null)) : ("#eeeeee"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_header_menu_color]\" value=\"";
        // line 274
        echo (((isset($context["basel_header_menu_color"]) ? $context["basel_header_menu_color"] : null)) ? ((isset($context["basel_header_menu_color"]) ? $context["basel_header_menu_color"] : null)) : ("#eeeeee"));
        echo "\" />
    </div> 
    </div>                 
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"(When using header 6)\">Search field color scheme</span></label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_search_scheme]\" class=\"form-control\">
        <option value=\"dark-search\"";
        // line 283
        if (((isset($context["basel_search_scheme"]) ? $context["basel_search_scheme"] : null) == "dark-search")) {
            echo " selected=\"selected\"";
        }
        echo ">Dark</option>
        <option value=\"light-search\"";
        // line 284
        if (((isset($context["basel_search_scheme"]) ? $context["basel_search_scheme"] : null) == "light-search")) {
            echo " selected=\"selected\"";
        }
        echo ">Light</option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Menu sale label</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 293
        echo (((isset($context["basel_menutag_sale_bg"]) ? $context["basel_menutag_sale_bg"] : null)) ? ((isset($context["basel_menutag_sale_bg"]) ? $context["basel_menutag_sale_bg"] : null)) : ("#D41212"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_menutag_sale_bg]\" value=\"";
        // line 294
        echo (((isset($context["basel_menutag_sale_bg"]) ? $context["basel_menutag_sale_bg"] : null)) ? ((isset($context["basel_menutag_sale_bg"]) ? $context["basel_menutag_sale_bg"] : null)) : ("#D41212"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Menu new label</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 303
        echo (((isset($context["basel_menutag_new_bg"]) ? $context["basel_menutag_new_bg"] : null)) ? ((isset($context["basel_menutag_new_bg"]) ? $context["basel_menutag_new_bg"] : null)) : ("#1daaa3"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_menutag_new_bg]\" value=\"";
        // line 304
        echo (((isset($context["basel_menutag_new_bg"]) ? $context["basel_menutag_new_bg"] : null)) ? ((isset($context["basel_menutag_new_bg"]) ? $context["basel_menutag_new_bg"] : null)) : ("#1daaa3"));
        echo "\" />
    </div> 
    </div>                  
</div>

<legend class=\"third\">Breadcrumbs (When Holding Titles)</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 315
        echo (((isset($context["basel_bc_bg_color"]) ? $context["basel_bc_bg_color"] : null)) ? ((isset($context["basel_bc_bg_color"]) ? $context["basel_bc_bg_color"] : null)) : ("#000000"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_bc_bg_color]\" value=\"";
        // line 316
        echo (((isset($context["basel_bc_bg_color"]) ? $context["basel_bc_bg_color"] : null)) ? ((isset($context["basel_bc_bg_color"]) ? $context["basel_bc_bg_color"] : null)) : ("#000000"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><br /><span data-toggle=\"tooltip\" title=\"Inline titles helper\">Background image</span></label>
    <div class=\"col-sm-10\">
    <div class=\"row\">
        <div class=\"col-sm-2\">
        <a href=\"\" id=\"thumb-bc-img\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 326
        echo (isset($context["bc_thumb"]) ? $context["bc_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
    <input type=\"hidden\" name=\"settings[basel][basel_bc_bg_img]\" value=\"";
        // line 327
        echo (isset($context["basel_bc_bg_img"]) ? $context["basel_bc_bg_img"] : null);
        echo "\" id=\"input-bc-img\" />
        </div>
        <div class=\"col-sm-2\">
        <label>Background Position</label>
        <select name=\"settings[basel][basel_bc_bg_img_pos]\" class=\"form-control\">
        <option value=\"top left\"";
        // line 332
        if (((isset($context["basel_bc_bg_img_pos"]) ? $context["basel_bc_bg_img_pos"] : null) == "top left")) {
            echo " selected=\"selected\"";
        }
        echo ">top left</option>
        <option value=\"top center\"";
        // line 333
        if (((isset($context["basel_bc_bg_img_pos"]) ? $context["basel_bc_bg_img_pos"] : null) == "top center")) {
            echo " selected=\"selected\"";
        }
        echo ">top center</option>
        <option value=\"top right\"";
        // line 334
        if (((isset($context["basel_bc_bg_img_pos"]) ? $context["basel_bc_bg_img_pos"] : null) == "top right")) {
            echo " selected=\"selected\"";
        }
        echo ">top right</option>
        <option value=\"center left\"";
        // line 335
        if (((isset($context["basel_bc_bg_img_pos"]) ? $context["basel_bc_bg_img_pos"] : null) == "center left")) {
            echo " selected=\"selected\"";
        }
        echo ">middle left</option>
        <option value=\"center center\"";
        // line 336
        if (((isset($context["basel_bc_bg_img_pos"]) ? $context["basel_bc_bg_img_pos"] : null) == "center center")) {
            echo " selected=\"selected\"";
        }
        echo ">middle center</option>
        <option value=\"center right\"";
        // line 337
        if (((isset($context["basel_bc_bg_img_pos"]) ? $context["basel_bc_bg_img_pos"] : null) == "center right")) {
            echo " selected=\"selected\"";
        }
        echo ">middle right</option>
        <option value=\"bottom left\"";
        // line 338
        if (((isset($context["basel_bc_bg_img_pos"]) ? $context["basel_bc_bg_img_pos"] : null) == "bottom left")) {
            echo " selected=\"selected\"";
        }
        echo ">bottom left</option>
        <option value=\"bottom center\"";
        // line 339
        if (((isset($context["basel_bc_bg_img_pos"]) ? $context["basel_bc_bg_img_pos"] : null) == "bottom center")) {
            echo " selected=\"selected\"";
        }
        echo ">bottom center</option>
        <option value=\"bottom right\"";
        // line 340
        if (((isset($context["basel_bc_bg_img_pos"]) ? $context["basel_bc_bg_img_pos"] : null) == "bottom right")) {
            echo " selected=\"selected\"";
        }
        echo ">bottom right</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Background Size</label>
        <select name=\"settings[basel][basel_bc_bg_img_size]\" class=\"form-control\">
        <option value=\"auto\"";
        // line 346
        if (((isset($context["basel_bc_bg_img_size"]) ? $context["basel_bc_bg_img_size"] : null) == "auto")) {
            echo " selected=\"selected\"";
        }
        echo ">auto</option>
        <option value=\"contain\"";
        // line 347
        if (((isset($context["basel_bc_bg_img_size"]) ? $context["basel_bc_bg_img_size"] : null) == "contain")) {
            echo " selected=\"selected\"";
        }
        echo ">contain</option>
        <option value=\"cover\"";
        // line 348
        if (((isset($context["basel_bc_bg_img_size"]) ? $context["basel_bc_bg_img_size"] : null) == "cover")) {
            echo " selected=\"selected\"";
        }
        echo ">cover</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Background Size</label>
        <select name=\"settings[basel][basel_bc_bg_img_repeat]\" class=\"form-control\">
        <option value=\"no-repeat\"";
        // line 354
        if (((isset($context["basel_bc_bg_img_repeat"]) ? $context["basel_bc_bg_img_repeat"] : null) == "no-repeat")) {
            echo " selected=\"selected\"";
        }
        echo ">no-repeat</option>
        <option value=\"repeat-x\"";
        // line 355
        if (((isset($context["basel_bc_bg_img_repeat"]) ? $context["basel_bc_bg_img_repeat"] : null) == "repeat-x")) {
            echo " selected=\"selected\"";
        }
        echo ">repeat-x (-)</option>
        <option value=\"repeat-y\"";
        // line 356
        if (((isset($context["basel_bc_bg_img_repeat"]) ? $context["basel_bc_bg_img_repeat"] : null) == "repeat-y")) {
            echo " selected=\"selected\"";
        }
        echo ">repeat-y (|)</option>
        <option value=\"repeat\"";
        // line 357
        if (((isset($context["basel_bc_bg_img_repeat"]) ? $context["basel_bc_bg_img_repeat"] : null) == "repeat")) {
            echo " selected=\"selected\"";
        }
        echo ">repeat</option>
        </select>
        </div>
        <div class=\"col-sm-2\">
        <label>Background Attachment</label>
        <select name=\"settings[basel][basel_bc_bg_img_att]\" class=\"form-control\">
        <option value=\"scroll\"";
        // line 363
        if (((isset($context["basel_bc_bg_img_att"]) ? $context["basel_bc_bg_img_att"] : null) == "scroll")) {
            echo " selected=\"selected\"";
        }
        echo ">scroll</option>
        <option value=\"local\"";
        // line 364
        if (((isset($context["basel_bc_bg_img_att"]) ? $context["basel_bc_bg_img_att"] : null) == "local")) {
            echo " selected=\"selected\"";
        }
        echo ">local</option>
        <option value=\"fixed\"";
        // line 365
        if (((isset($context["basel_bc_bg_img_att"]) ? $context["basel_bc_bg_img_att"] : null) == "fixed")) {
            echo " selected=\"selected\"";
        }
        echo ">fixed</option>
        </select>
        </div>        
            
    </div>
    </div>                   
</div>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 376
        echo (((isset($context["basel_bc_color"]) ? $context["basel_bc_color"] : null)) ? ((isset($context["basel_bc_color"]) ? $context["basel_bc_color"] : null)) : ("#ffffff"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_bc_color]\" value=\"";
        // line 377
        echo (((isset($context["basel_bc_color"]) ? $context["basel_bc_color"] : null)) ? ((isset($context["basel_bc_color"]) ? $context["basel_bc_color"] : null)) : ("#ffffff"));
        echo "\" />
    </div> 
    </div>                  
</div>



<legend class=\"third\">Content Area</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Color when hovering links etc\">Primary accent color</span></label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 389
        echo (((isset($context["basel_primary_accent_color"]) ? $context["basel_primary_accent_color"] : null)) ? ((isset($context["basel_primary_accent_color"]) ? $context["basel_primary_accent_color"] : null)) : ("#1daaa3"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_primary_accent_color]\" value=\"";
        // line 390
        echo (((isset($context["basel_primary_accent_color"]) ? $context["basel_primary_accent_color"] : null)) ? ((isset($context["basel_primary_accent_color"]) ? $context["basel_primary_accent_color"] : null)) : ("#1daaa3"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"On produts in product listings and product pages\">Sale badge background</span></label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 399
        echo (((isset($context["basel_salebadge_bg"]) ? $context["basel_salebadge_bg"] : null)) ? ((isset($context["basel_salebadge_bg"]) ? $context["basel_salebadge_bg"] : null)) : ("#1daaa3"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_salebadge_bg]\" value=\"";
        // line 400
        echo (((isset($context["basel_salebadge_bg"]) ? $context["basel_salebadge_bg"] : null)) ? ((isset($context["basel_salebadge_bg"]) ? $context["basel_salebadge_bg"] : null)) : ("#1daaa3"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"On produts in product listings and product pages\">Sale badge color</span></label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 409
        echo (((isset($context["basel_salebadge_color"]) ? $context["basel_salebadge_color"] : null)) ? ((isset($context["basel_salebadge_color"]) ? $context["basel_salebadge_color"] : null)) : ("#ffffff"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_salebadge_color]\" value=\"";
        // line 410
        echo (((isset($context["basel_salebadge_color"]) ? $context["basel_salebadge_color"] : null)) ? ((isset($context["basel_salebadge_color"]) ? $context["basel_salebadge_color"] : null)) : ("#ffffff"));
        echo "\" />
    </div> 
    </div>                 
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"On produts in product listings and product pages\">New badge background</span></label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 419
        echo (((isset($context["basel_newbadge_bg"]) ? $context["basel_newbadge_bg"] : null)) ? ((isset($context["basel_newbadge_bg"]) ? $context["basel_newbadge_bg"] : null)) : ("#ffffff"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_newbadge_bg]\" value=\"";
        // line 420
        echo (((isset($context["basel_newbadge_bg"]) ? $context["basel_newbadge_bg"] : null)) ? ((isset($context["basel_newbadge_bg"]) ? $context["basel_newbadge_bg"] : null)) : ("#ffffff"));
        echo "\" />
    </div> 
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"On produts in product listings and product pages\">New badge color</span></label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 429
        echo (((isset($context["basel_newbadge_color"]) ? $context["basel_newbadge_color"] : null)) ? ((isset($context["basel_newbadge_color"]) ? $context["basel_newbadge_color"] : null)) : ("#111111"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_newbadge_color]\" value=\"";
        // line 430
        echo (((isset($context["basel_newbadge_color"]) ? $context["basel_newbadge_color"] : null)) ? ((isset($context["basel_newbadge_color"]) ? $context["basel_newbadge_color"] : null)) : ("#111111"));
        echo "\" />
    </div> 
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Price color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 439
        echo (((isset($context["basel_price_color"]) ? $context["basel_price_color"] : null)) ? ((isset($context["basel_price_color"]) ? $context["basel_price_color"] : null)) : ("#1daaa3"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_price_color]\" value=\"";
        // line 440
        echo (((isset($context["basel_price_color"]) ? $context["basel_price_color"] : null)) ? ((isset($context["basel_price_color"]) ? $context["basel_price_color"] : null)) : ("#1daaa3"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"When using vertical mega menu modules, or when using header alternative 5\">Vertical menu background</span></label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 449
        echo (((isset($context["basel_vertical_menu_bg"]) ? $context["basel_vertical_menu_bg"] : null)) ? ((isset($context["basel_vertical_menu_bg"]) ? $context["basel_vertical_menu_bg"] : null)) : ("#212121"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_vertical_menu_bg]\" value=\"";
        // line 450
        echo (((isset($context["basel_vertical_menu_bg"]) ? $context["basel_vertical_menu_bg"] : null)) ? ((isset($context["basel_vertical_menu_bg"]) ? $context["basel_vertical_menu_bg"] : null)) : ("#212121"));
        echo "\" />
    </div> 
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"When using vertical mega menu modules, or when using header alternative 5\">Vertical menu background on hover</span></label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 459
        echo (((isset($context["basel_vertical_menu_bg_hover"]) ? $context["basel_vertical_menu_bg_hover"] : null)) ? ((isset($context["basel_vertical_menu_bg_hover"]) ? $context["basel_vertical_menu_bg_hover"] : null)) : ("#fbbc34"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_vertical_menu_bg_hover]\" value=\"";
        // line 460
        echo (((isset($context["basel_vertical_menu_bg_hover"]) ? $context["basel_vertical_menu_bg_hover"] : null)) ? ((isset($context["basel_vertical_menu_bg_hover"]) ? $context["basel_vertical_menu_bg_hover"] : null)) : ("#fbbc34"));
        echo "\" />
    </div> 
    </div>                  
</div>


<legend class=\"third\">Buttons</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Default buttons: Background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 471
        echo (((isset($context["basel_default_btn_bg"]) ? $context["basel_default_btn_bg"] : null)) ? ((isset($context["basel_default_btn_bg"]) ? $context["basel_default_btn_bg"] : null)) : ("#000000"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_default_btn_bg]\" value=\"";
        // line 472
        echo (((isset($context["basel_default_btn_bg"]) ? $context["basel_default_btn_bg"] : null)) ? ((isset($context["basel_default_btn_bg"]) ? $context["basel_default_btn_bg"] : null)) : ("#000000"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Default buttons: Color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 481
        echo (((isset($context["basel_default_btn_color"]) ? $context["basel_default_btn_color"] : null)) ? ((isset($context["basel_default_btn_color"]) ? $context["basel_default_btn_color"] : null)) : ("#ffffff"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_default_btn_color]\" value=\"";
        // line 482
        echo (((isset($context["basel_default_btn_color"]) ? $context["basel_default_btn_color"] : null)) ? ((isset($context["basel_default_btn_color"]) ? $context["basel_default_btn_color"] : null)) : ("#ffffff"));
        echo "\" />
    </div> 
    </div>                  
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Default buttons: Hover background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 491
        echo (((isset($context["basel_default_btn_bg_hover"]) ? $context["basel_default_btn_bg_hover"] : null)) ? ((isset($context["basel_default_btn_bg_hover"]) ? $context["basel_default_btn_bg_hover"] : null)) : ("#3e3e3e"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_default_btn_bg_hover]\" value=\"";
        // line 492
        echo (((isset($context["basel_default_btn_bg_hover"]) ? $context["basel_default_btn_bg_hover"] : null)) ? ((isset($context["basel_default_btn_bg_hover"]) ? $context["basel_default_btn_bg_hover"] : null)) : ("#3e3e3e"));
        echo "\" />
    </div> 
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Default buttons: Hover color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 501
        echo (((isset($context["basel_default_btn_color_hover"]) ? $context["basel_default_btn_color_hover"] : null)) ? ((isset($context["basel_default_btn_color_hover"]) ? $context["basel_default_btn_color_hover"] : null)) : ("#ffffff"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_default_btn_color_hover]\" value=\"";
        // line 502
        echo (((isset($context["basel_default_btn_color_hover"]) ? $context["basel_default_btn_color_hover"] : null)) ? ((isset($context["basel_default_btn_color_hover"]) ? $context["basel_default_btn_color_hover"] : null)) : ("#ffffff"));
        echo "\" />
    </div> 
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Action buttons: Background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 511
        echo (((isset($context["basel_contrast_btn_bg"]) ? $context["basel_contrast_btn_bg"] : null)) ? ((isset($context["basel_contrast_btn_bg"]) ? $context["basel_contrast_btn_bg"] : null)) : ("#1daaa3"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_contrast_btn_bg]\" value=\"";
        // line 512
        echo (((isset($context["basel_contrast_btn_bg"]) ? $context["basel_contrast_btn_bg"] : null)) ? ((isset($context["basel_contrast_btn_bg"]) ? $context["basel_contrast_btn_bg"] : null)) : ("#1daaa3"));
        echo "\" />
    </div> 
    </div>                   
</div>


<legend class=\"third\">Footer</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Background</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 523
        echo (((isset($context["basel_footer_bg"]) ? $context["basel_footer_bg"] : null)) ? ((isset($context["basel_footer_bg"]) ? $context["basel_footer_bg"] : null)) : ("#000000"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_footer_bg]\" value=\"";
        // line 524
        echo (((isset($context["basel_footer_bg"]) ? $context["basel_footer_bg"] : null)) ? ((isset($context["basel_footer_bg"]) ? $context["basel_footer_bg"] : null)) : ("#000000"));
        echo "\" />
    </div> 
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 533
        echo (((isset($context["basel_footer_color"]) ? $context["basel_footer_color"] : null)) ? ((isset($context["basel_footer_color"]) ? $context["basel_footer_color"] : null)) : ("#ffffff"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_footer_color]\" value=\"";
        // line 534
        echo (((isset($context["basel_footer_color"]) ? $context["basel_footer_color"] : null)) ? ((isset($context["basel_footer_color"]) ? $context["basel_footer_color"] : null)) : ("#ffffff"));
        echo "\" />
    </div> 
    </div>               
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Heading / Links separator color</label>
    <div class=\"col-sm-10\">
    <div class=\"input-group form-inline colorfield\">
    <span class=\"input-group-addon\"><i style=\"background:";
        // line 543
        echo (((isset($context["basel_footer_h5_sep"]) ? $context["basel_footer_h5_sep"] : null)) ? ((isset($context["basel_footer_h5_sep"]) ? $context["basel_footer_h5_sep"] : null)) : ("#cccccc"));
        echo "\"></i></span>
    <input class=\"form-control\" name=\"settings[basel][basel_footer_h5_sep]\" value=\"";
        // line 544
        echo (((isset($context["basel_footer_h5_sep"]) ? $context["basel_footer_h5_sep"] : null)) ? ((isset($context["basel_footer_h5_sep"]) ? $context["basel_footer_h5_sep"] : null)) : ("#cccccc"));
        echo "\" />
    </div> 
    </div>                
</div>
</div><!-- #custom_design_holder -->
";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/styles.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1117 => 544,  1113 => 543,  1101 => 534,  1097 => 533,  1085 => 524,  1081 => 523,  1067 => 512,  1063 => 511,  1051 => 502,  1047 => 501,  1035 => 492,  1031 => 491,  1019 => 482,  1015 => 481,  1003 => 472,  999 => 471,  985 => 460,  981 => 459,  969 => 450,  965 => 449,  953 => 440,  949 => 439,  937 => 430,  933 => 429,  921 => 420,  917 => 419,  905 => 410,  901 => 409,  889 => 400,  885 => 399,  873 => 390,  869 => 389,  854 => 377,  850 => 376,  834 => 365,  828 => 364,  822 => 363,  811 => 357,  805 => 356,  799 => 355,  793 => 354,  782 => 348,  776 => 347,  770 => 346,  759 => 340,  753 => 339,  747 => 338,  741 => 337,  735 => 336,  729 => 335,  723 => 334,  717 => 333,  711 => 332,  703 => 327,  697 => 326,  684 => 316,  680 => 315,  666 => 304,  662 => 303,  650 => 294,  646 => 293,  632 => 284,  626 => 283,  614 => 274,  610 => 273,  598 => 264,  594 => 263,  581 => 253,  577 => 252,  565 => 243,  561 => 242,  549 => 233,  545 => 232,  532 => 222,  528 => 221,  516 => 212,  512 => 211,  499 => 201,  495 => 200,  483 => 191,  479 => 190,  460 => 176,  454 => 175,  448 => 174,  437 => 168,  431 => 167,  425 => 166,  419 => 165,  408 => 159,  402 => 158,  396 => 157,  385 => 151,  379 => 150,  373 => 149,  367 => 148,  361 => 147,  355 => 146,  349 => 145,  343 => 144,  337 => 143,  329 => 138,  323 => 137,  309 => 126,  305 => 125,  290 => 117,  281 => 113,  275 => 112,  262 => 104,  256 => 103,  244 => 96,  238 => 95,  226 => 88,  220 => 87,  207 => 79,  201 => 78,  195 => 77,  189 => 76,  183 => 75,  177 => 74,  162 => 64,  156 => 63,  150 => 62,  138 => 53,  126 => 46,  120 => 45,  106 => 36,  100 => 35,  94 => 34,  81 => 26,  75 => 25,  62 => 17,  55 => 15,  48 => 13,  41 => 11,  34 => 9,  27 => 7,  19 => 1,);
    }
}
/* <legend>Styles & Colors</legend>*/
/* */
/* <legend class="sub">General</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Shopping cart icon</label>*/
/*     <div class="col-sm-10 image-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_cart_icon]" value="global-cart-bag" {% if basel_cart_icon == 'global-cart-bag' %} checked="checked"{% endif %} /><span><i class="icon-bag"></i></span></label>*/
/*     */
/*     <label><input type="radio" name="settings[basel][basel_cart_icon]" value="global-cart-basket" {% if basel_cart_icon == 'global-cart-basket' %} checked="checked"{% endif %} /><span><i class="icon-basket"></i></span></label>*/
/*     */
/*     <label><input type="radio" name="settings[basel][basel_cart_icon]" value="global-cart-handbag" {% if basel_cart_icon == 'global-cart-handbag' %} checked="checked"{% endif %} /><span><i class="icon-handbag"></i></span></label>*/
/*     */
/*     <label><input type="radio" name="settings[basel][basel_cart_icon]" value="global-cart-briefcase" {% if basel_cart_icon == 'global-cart-briefcase' %} checked="checked"{% endif %} /><span><i class="icon-briefcase"></i></span></label>*/
/*     */
/*     <label><input type="radio" name="settings[basel][basel_cart_icon]" value="global-cart-shoppingbag" {% if basel_cart_icon == 'global-cart-shoppingbag' %} checked="checked"{% endif %} /><span><i class="icon-shopping-bag"></i></span></label>*/
/*     */
/*     <label><input type="radio" name="settings[basel][basel_cart_icon]" value="global-cart-shoppingbasket" {% if basel_cart_icon == 'global-cart-shoppingbasket' %} checked="checked"{% endif %} /><span><i class="icon-shopping-basket"></i></span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Layout</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Layout Style</label>*/
/*     <div class="col-sm-10 toggle-btn both-blue">*/
/*     <label><input type="radio" name="settings[basel][basel_main_layout]" value="0" {% if basel_main_layout == '0' %} checked="checked"{% endif %} /><span>Full width</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_main_layout]" value="1" {% if basel_main_layout == '1' %} checked="checked"{% endif %} /><span>Boxed width</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Content width</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_content_width]" class="form-control">*/
/*         <option value="narrow_container"{% if basel_content_width == 'narrow_container' %} selected="selected"{% endif %}>Narrow (1060px)</option>*/
/*         <option value=""{% if basel_content_width == '' %} selected="selected"{% endif %}>Normal (1170px)</option>*/
/*         <option value="wide_container"{% if basel_content_width == 'wide_container' %} selected="selected"{% endif %}>Wide (1280px)</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Sticky Columns</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Sticky columns stays within the viewport when scrolling down">Sticky Columns</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_sticky_columns]" value="0" {% if basel_sticky_columns == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_sticky_columns]" value="1" {% if basel_sticky_columns == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Use offset if sticky header is enabled to avoid overlapping columns/header. Enter a value in pixels, for example 100">Sticky Offset Top</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <input class="form-control" style="width:120px" name="settings[basel][basel_sticky_columns_offset]" value="{{ basel_sticky_columns_offset ? basel_sticky_columns_offset : '100' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Widget Titles</legend>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Select heading style on modules">Widget heading style</span></label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_widget_title_style]" class="form-control">*/
/*         <option value="0"{% if basel_widget_title_style == '0' %} selected="selected"{% endif %}>Style 1 - (Cross Separator)</option>*/
/*         <option value="2"{% if basel_widget_title_style == '2' %} selected="selected"{% endif %}>Style 2 - (Line Separator)</option>*/
/*         <option value="3"{% if basel_widget_title_style == '3' %} selected="selected"{% endif %}>Style 3 - (Bordered Title, No Separator)</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Product Listing Style</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Select how to show products in product listings, like category pages, related products etc.">Product listing style</span></label>*/
/*     <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_list_style]" class="form-control">*/
/*         <option value="1"{% if basel_list_style == '1' %} selected="selected"{% endif %}>Style 1 - Default style</option>*/
/*         <option value="2"{% if basel_list_style == '2' %} selected="selected"{% endif %}>Style 2 - Action buttons slide up on hover</option>*/
/*         <option value="3"{% if basel_list_style == '3' %} selected="selected"{% endif %}>Style 3 - Dark image overlay on hover</option>*/
/*         <option value="4"{% if basel_list_style == '4' %} selected="selected"{% endif %}>Style 4 - Dark image overlay on hover alt 2</option>*/
/*         <option value="1 names-c"{% if basel_list_style == '1 names-c' %} selected="selected"{% endif %}>Style 5 - Center aligned</option>*/
/*         <option value="6"{% if basel_list_style == '6' %} selected="selected"{% endif %}>Style 6 - Center aligned with button</option>*/
/*     </select>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Show the first additional image when hovering a product">Swap image on hover</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_thumb_swap]" value="0" {% if basel_thumb_swap == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_thumb_swap]" value="1" {% if basel_thumb_swap == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Keep the product names in listings within one line (using only CSS)">Cut product names</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_cut_names]" value="0" {% if basel_cut_names == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_cut_names]" value="1" {% if basel_cut_names == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="On smaller mobile devices, select to view 1 or 2 items per row">Minimum items per row</span></label>*/
/*     <div class="col-sm-10 toggle-btn both-blue">*/
/*     <label><input type="radio" name="settings[basel][items_mobile_fw]" value="1" {% if items_mobile_fw == '1' %} checked="checked"{% endif %} /><span>1 item</span></label>*/
/*     <label><input type="radio" name="settings[basel][items_mobile_fw]" value="0" {% if items_mobile_fw == '0' %} checked="checked"{% endif %} /><span>2 items</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend>Custom Color Scheme</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Override default colors</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_design_status]" class="design-select" value="0" {% if basel_design_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_design_status]" class="design-select" value="1" {% if basel_design_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div id="custom_design_holder"{% if basel_design_status %} style="display:block"{% else %} style="display:none"{% endif %}>*/
/* */
/* <legend class="third">Body Background (when using boxed layout)</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_body_bg_color ? basel_body_bg_color : '#ececec' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_body_bg_color]" value="{{ basel_body_bg_color ? basel_body_bg_color : '#ececec' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Background Image</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="row">*/
/*         <div class="col-sm-2">*/
/*         <a href="" id="thumb-bc-img" data-toggle="image" class="img-thumbnail"><img src="{{ body_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*     <input type="hidden" name="settings[basel][basel_body_bg_img]" value="{{ basel_body_bg_img }}" id="input-body-img" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Background Position</label>*/
/*         <select name="settings[basel][basel_body_bg_img_pos]" class="form-control">*/
/*         <option value="top left"{% if basel_body_bg_img_pos == 'top left' %} selected="selected"{% endif %}>top left</option>*/
/*         <option value="top center"{% if basel_body_bg_img_pos == 'top center' %} selected="selected"{% endif %}>top center</option>*/
/*         <option value="top right"{% if basel_body_bg_img_pos == 'top right' %} selected="selected"{% endif %}>top right</option>*/
/*         <option value="center left"{% if basel_body_bg_img_pos == 'center left' %} selected="selected"{% endif %}>middle left</option>*/
/*         <option value="center center"{% if basel_body_bg_img_pos == 'center center' %} selected="selected"{% endif %}>middle center</option>*/
/*         <option value="center right"{% if basel_body_bg_img_pos == 'center right' %} selected="selected"{% endif %}>middle right</option>*/
/*         <option value="bottom left"{% if basel_body_bg_img_pos == 'bottom left' %} selected="selected"{% endif %}>bottom left</option>*/
/*         <option value="bottom center"{% if basel_body_bg_img_pos == 'bottom center' %} selected="selected"{% endif %}>bottom center</option>*/
/*         <option value="bottom right"{% if basel_body_bg_img_pos == 'bottom right' %} selected="selected"{% endif %}>bottom right</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Background Size</label>*/
/*         <select name="settings[basel][basel_body_bg_img_size]" class="form-control">*/
/*         <option value="auto"{% if basel_body_bg_img_size == 'auto' %} selected="selected"{% endif %}>auto</option>*/
/*         <option value="contain"{% if basel_body_bg_img_size == 'contain' %} selected="selected"{% endif %}>contain</option>*/
/*         <option value="cover"{% if basel_body_bg_img_size == 'cover' %} selected="selected"{% endif %}>cover</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Background Size</label>*/
/*         <select name="settings[basel][basel_body_bg_img_repeat]" class="form-control">*/
/*         <option value="no-repeat"{% if basel_body_bg_img_repeat == 'no-repeat' %} selected="selected"{% endif %}>no-repeat</option>*/
/*         <option value="repeat-x"{% if basel_body_bg_img_repeat == 'repeat-x' %} selected="selected"{% endif %}>repeat-x (-)</option>*/
/*         <option value="repeat-y"{% if basel_body_bg_img_repeat == 'repeat-y' %} selected="selected"{% endif %}>repeat-y (|)</option>*/
/*         <option value="repeat"{% if basel_body_bg_img_repeat == 'repeat' %} selected="selected"{% endif %}>repeat</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Background Attachment</label>*/
/*         <select name="settings[basel][basel_body_bg_img_att]" class="form-control">*/
/*         <option value="scroll"{% if basel_body_bg_img_att == 'scroll' %} selected="selected"{% endif %}>scroll</option>*/
/*         <option value="local"{% if basel_body_bg_img_att == 'local' %} selected="selected"{% endif %}>local</option>*/
/*         <option value="fixed"{% if basel_body_bg_img_att == 'fixed' %} selected="selected"{% endif %}>fixed</option>*/
/*         </select>*/
/*         </div>        */
/*             */
/*     </div>*/
/*     </div>                   */
/* </div>*/
/* */
/* */
/* <legend class="third">Top Line Promo Message</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_top_note_bg ? basel_top_note_bg : '#000000' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_top_note_bg]" value="{{ basel_top_note_bg ? basel_top_note_bg : '#000000' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_top_note_color ? basel_top_note_color : '#eeeeee' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_top_note_color]" value="{{ basel_top_note_color ? basel_top_note_color : '#eeeeee' }}" />*/
/*     </div> */
/*     </div>                 */
/* </div>*/
/* */
/* <legend class="third">Top Line</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_top_line_bg ? basel_top_line_bg : '#1daaa3' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_top_line_bg]" value="{{ basel_top_line_bg ? basel_top_line_bg : '#1daaa3' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_top_line_color ? basel_top_line_color : '#ffffff' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_top_line_color]" value="{{ basel_top_line_color ? basel_top_line_color : '#ffffff' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="third">Header Area</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_header_bg ? basel_header_bg : '#ffffff' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_header_bg]" value="{{ basel_header_bg ? basel_header_bg : '#ffffff' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_header_color ? basel_header_color : '#000000' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_header_color]" value="{{ basel_header_color ? basel_header_color : '#000000' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Color on details in header, for example product counters">Header accent color</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_header_accent ? basel_header_accent : '#1daaa3' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_header_accent]" value="{{ basel_header_accent ? basel_header_accent : '#1daaa3' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <legend class="third">Menu</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_header_menu_bg ? basel_header_menu_bg : '#111111' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_header_menu_bg]" value="{{ basel_header_menu_bg ? basel_header_menu_bg : '#111111' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_header_menu_color ? basel_header_menu_color : '#eeeeee' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_header_menu_color]" value="{{ basel_header_menu_color ? basel_header_menu_color : '#eeeeee' }}" />*/
/*     </div> */
/*     </div>                 */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="(When using header 6)">Search field color scheme</span></label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_search_scheme]" class="form-control">*/
/*         <option value="dark-search"{% if basel_search_scheme == 'dark-search' %} selected="selected"{% endif %}>Dark</option>*/
/*         <option value="light-search"{% if basel_search_scheme == 'light-search' %} selected="selected"{% endif %}>Light</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Menu sale label</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_menutag_sale_bg ? basel_menutag_sale_bg : '#D41212' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_menutag_sale_bg]" value="{{ basel_menutag_sale_bg ? basel_menutag_sale_bg : '#D41212' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Menu new label</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_menutag_new_bg ? basel_menutag_new_bg : '#1daaa3' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_menutag_new_bg]" value="{{ basel_menutag_new_bg ? basel_menutag_new_bg : '#1daaa3' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <legend class="third">Breadcrumbs (When Holding Titles)</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_bc_bg_color ? basel_bc_bg_color : '#000000' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_bc_bg_color]" value="{{ basel_bc_bg_color ? basel_bc_bg_color : '#000000' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><br /><span data-toggle="tooltip" title="Inline titles helper">Background image</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="row">*/
/*         <div class="col-sm-2">*/
/*         <a href="" id="thumb-bc-img" data-toggle="image" class="img-thumbnail"><img src="{{ bc_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*     <input type="hidden" name="settings[basel][basel_bc_bg_img]" value="{{ basel_bc_bg_img }}" id="input-bc-img" />*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Background Position</label>*/
/*         <select name="settings[basel][basel_bc_bg_img_pos]" class="form-control">*/
/*         <option value="top left"{% if basel_bc_bg_img_pos == 'top left' %} selected="selected"{% endif %}>top left</option>*/
/*         <option value="top center"{% if basel_bc_bg_img_pos == 'top center' %} selected="selected"{% endif %}>top center</option>*/
/*         <option value="top right"{% if basel_bc_bg_img_pos == 'top right' %} selected="selected"{% endif %}>top right</option>*/
/*         <option value="center left"{% if basel_bc_bg_img_pos == 'center left' %} selected="selected"{% endif %}>middle left</option>*/
/*         <option value="center center"{% if basel_bc_bg_img_pos == 'center center' %} selected="selected"{% endif %}>middle center</option>*/
/*         <option value="center right"{% if basel_bc_bg_img_pos == 'center right' %} selected="selected"{% endif %}>middle right</option>*/
/*         <option value="bottom left"{% if basel_bc_bg_img_pos == 'bottom left' %} selected="selected"{% endif %}>bottom left</option>*/
/*         <option value="bottom center"{% if basel_bc_bg_img_pos == 'bottom center' %} selected="selected"{% endif %}>bottom center</option>*/
/*         <option value="bottom right"{% if basel_bc_bg_img_pos == 'bottom right' %} selected="selected"{% endif %}>bottom right</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Background Size</label>*/
/*         <select name="settings[basel][basel_bc_bg_img_size]" class="form-control">*/
/*         <option value="auto"{% if basel_bc_bg_img_size == 'auto' %} selected="selected"{% endif %}>auto</option>*/
/*         <option value="contain"{% if basel_bc_bg_img_size == 'contain' %} selected="selected"{% endif %}>contain</option>*/
/*         <option value="cover"{% if basel_bc_bg_img_size == 'cover' %} selected="selected"{% endif %}>cover</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Background Size</label>*/
/*         <select name="settings[basel][basel_bc_bg_img_repeat]" class="form-control">*/
/*         <option value="no-repeat"{% if basel_bc_bg_img_repeat == 'no-repeat' %} selected="selected"{% endif %}>no-repeat</option>*/
/*         <option value="repeat-x"{% if basel_bc_bg_img_repeat == 'repeat-x' %} selected="selected"{% endif %}>repeat-x (-)</option>*/
/*         <option value="repeat-y"{% if basel_bc_bg_img_repeat == 'repeat-y' %} selected="selected"{% endif %}>repeat-y (|)</option>*/
/*         <option value="repeat"{% if basel_bc_bg_img_repeat == 'repeat' %} selected="selected"{% endif %}>repeat</option>*/
/*         </select>*/
/*         </div>*/
/*         <div class="col-sm-2">*/
/*         <label>Background Attachment</label>*/
/*         <select name="settings[basel][basel_bc_bg_img_att]" class="form-control">*/
/*         <option value="scroll"{% if basel_bc_bg_img_att == 'scroll' %} selected="selected"{% endif %}>scroll</option>*/
/*         <option value="local"{% if basel_bc_bg_img_att == 'local' %} selected="selected"{% endif %}>local</option>*/
/*         <option value="fixed"{% if basel_bc_bg_img_att == 'fixed' %} selected="selected"{% endif %}>fixed</option>*/
/*         </select>*/
/*         </div>        */
/*             */
/*     </div>*/
/*     </div>                   */
/* </div>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_bc_color ? basel_bc_color : '#ffffff' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_bc_color]" value="{{ basel_bc_color ? basel_bc_color : '#ffffff' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* */
/* */
/* <legend class="third">Content Area</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Color when hovering links etc">Primary accent color</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_primary_accent_color ? basel_primary_accent_color : '#1daaa3' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_primary_accent_color]" value="{{ basel_primary_accent_color ? basel_primary_accent_color : '#1daaa3' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="On produts in product listings and product pages">Sale badge background</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_salebadge_bg ? basel_salebadge_bg : '#1daaa3' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_salebadge_bg]" value="{{ basel_salebadge_bg ? basel_salebadge_bg : '#1daaa3' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="On produts in product listings and product pages">Sale badge color</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_salebadge_color ? basel_salebadge_color : '#ffffff' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_salebadge_color]" value="{{ basel_salebadge_color ? basel_salebadge_color : '#ffffff' }}" />*/
/*     </div> */
/*     </div>                 */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="On produts in product listings and product pages">New badge background</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_newbadge_bg ? basel_newbadge_bg : '#ffffff' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_newbadge_bg]" value="{{ basel_newbadge_bg ? basel_newbadge_bg : '#ffffff' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="On produts in product listings and product pages">New badge color</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_newbadge_color ? basel_newbadge_color : '#111111' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_newbadge_color]" value="{{ basel_newbadge_color ? basel_newbadge_color : '#111111' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Price color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_price_color ? basel_price_color : '#1daaa3' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_price_color]" value="{{ basel_price_color ? basel_price_color : '#1daaa3' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="When using vertical mega menu modules, or when using header alternative 5">Vertical menu background</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_vertical_menu_bg ? basel_vertical_menu_bg : '#212121' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_vertical_menu_bg]" value="{{ basel_vertical_menu_bg ? basel_vertical_menu_bg : '#212121' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="When using vertical mega menu modules, or when using header alternative 5">Vertical menu background on hover</span></label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_vertical_menu_bg_hover ? basel_vertical_menu_bg_hover : '#fbbc34' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_vertical_menu_bg_hover]" value="{{ basel_vertical_menu_bg_hover ? basel_vertical_menu_bg_hover : '#fbbc34' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* */
/* <legend class="third">Buttons</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Default buttons: Background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_default_btn_bg ? basel_default_btn_bg : '#000000' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_default_btn_bg]" value="{{ basel_default_btn_bg ? basel_default_btn_bg : '#000000' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Default buttons: Color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_default_btn_color ? basel_default_btn_color : '#ffffff' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_default_btn_color]" value="{{ basel_default_btn_color ? basel_default_btn_color : '#ffffff' }}" />*/
/*     </div> */
/*     </div>                  */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Default buttons: Hover background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_default_btn_bg_hover ? basel_default_btn_bg_hover : '#3e3e3e' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_default_btn_bg_hover]" value="{{ basel_default_btn_bg_hover ? basel_default_btn_bg_hover : '#3e3e3e' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Default buttons: Hover color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_default_btn_color_hover ? basel_default_btn_color_hover : '#ffffff' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_default_btn_color_hover]" value="{{ basel_default_btn_color_hover ? basel_default_btn_color_hover : '#ffffff' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Action buttons: Background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_contrast_btn_bg ? basel_contrast_btn_bg : '#1daaa3' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_contrast_btn_bg]" value="{{ basel_contrast_btn_bg ? basel_contrast_btn_bg : '#1daaa3' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* */
/* <legend class="third">Footer</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Background</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_footer_bg ? basel_footer_bg : '#000000' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_footer_bg]" value="{{ basel_footer_bg ? basel_footer_bg : '#000000' }}" />*/
/*     </div> */
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_footer_color ? basel_footer_color : '#ffffff' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_footer_color]" value="{{ basel_footer_color ? basel_footer_color : '#ffffff' }}" />*/
/*     </div> */
/*     </div>               */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Heading / Links separator color</label>*/
/*     <div class="col-sm-10">*/
/*     <div class="input-group form-inline colorfield">*/
/*     <span class="input-group-addon"><i style="background:{{ basel_footer_h5_sep ? basel_footer_h5_sep : '#cccccc' }}"></i></span>*/
/*     <input class="form-control" name="settings[basel][basel_footer_h5_sep]" value="{{ basel_footer_h5_sep ? basel_footer_h5_sep : '#cccccc' }}" />*/
/*     </div> */
/*     </div>                */
/* </div>*/
/* </div><!-- #custom_design_holder -->*/
/* */
