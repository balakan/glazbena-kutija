<?php

/* basel/template/common/basel_search.twig */
class __TwigTemplate_5e169064f2c3437e236f616d4117c581f384990006e20a34dd2eb3ce9179ad72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"search-field\">
<input type=\"text\" name=\"search\" value=\"";
        // line 2
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["basel_text_search"]) ? $context["basel_text_search"] : null);
        echo "\" class=\"form-control main-search-input\" />
<span class=\"btn btn-contrast do-search main\">";
        // line 3
        echo (isset($context["basel_text_search"]) ? $context["basel_text_search"] : null);
        echo "</span>
</div>
<script>
\$(document).ready(function() {
\$('.search-holder-mobile input[name=\\'search-mobile\\']').attr(\"placeholder\", \"";
        // line 7
        echo (isset($context["basel_text_search"]) ? $context["basel_text_search"] : null);
        echo "\").attr(\"value\", \"";
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "\");
});
</script>";
    }

    public function getTemplateName()
    {
        return "basel/template/common/basel_search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 7,  28 => 3,  22 => 2,  19 => 1,);
    }
}
/* <div class="search-field">*/
/* <input type="text" name="search" value="{{ search }}" placeholder="{{ basel_text_search }}" class="form-control main-search-input" />*/
/* <span class="btn btn-contrast do-search main">{{ basel_text_search }}</span>*/
/* </div>*/
/* <script>*/
/* $(document).ready(function() {*/
/* $('.search-holder-mobile input[name=\'search-mobile\']').attr("placeholder", "{{ basel_text_search }}").attr("value", "{{ search }}");*/
/* });*/
/* </script>*/
