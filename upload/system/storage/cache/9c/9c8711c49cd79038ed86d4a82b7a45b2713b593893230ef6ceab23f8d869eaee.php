<?php

/* extension/basel/panel_tabs/custom-css.twig */
class __TwigTemplate_4b17703fe13a6c4b6eadafc64b064b2c005017762ac0b8e47e07e170dcac3950 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Custom CSS</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Custom CSS Status</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_custom_css_status]\" class=\"custom-css-select\" value=\"0\" ";
        // line 6
        if (((isset($context["basel_custom_css_status"]) ? $context["basel_custom_css_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_custom_css_status]\" class=\"custom-css-select\" value=\"1\" ";
        // line 7
        if (((isset($context["basel_custom_css_status"]) ? $context["basel_custom_css_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div id=\"custom_css_holder\"";
        // line 11
        if ((isset($context["basel_custom_css_status"]) ? $context["basel_custom_css_status"] : null)) {
            echo " style=\"display:block\"";
        } else {
            echo " style=\"display:none\"";
        }
        echo ">
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">CSS</label>
    <div class=\"col-sm-10\">
    <textarea name=\"settings[basel][basel_custom_css]\" class=\"form-control code\">";
        // line 15
        echo (((isset($context["basel_custom_css"]) ? $context["basel_custom_css"] : null)) ? ((isset($context["basel_custom_css"]) ? $context["basel_custom_css"] : null)) : (""));
        echo "</textarea>
    </div>                   
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/custom-css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 15,  41 => 11,  32 => 7,  26 => 6,  19 => 1,);
    }
}
/* <legend>Custom CSS</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Custom CSS Status</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_custom_css_status]" class="custom-css-select" value="0" {% if basel_custom_css_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_custom_css_status]" class="custom-css-select" value="1" {% if basel_custom_css_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div id="custom_css_holder"{% if basel_custom_css_status %} style="display:block"{% else %} style="display:none"{% endif %}>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">CSS</label>*/
/*     <div class="col-sm-10">*/
/*     <textarea name="settings[basel][basel_custom_css]" class="form-control code">{{ basel_custom_css ? basel_custom_css }}</textarea>*/
/*     </div>                   */
/* </div>*/
/* </div>*/
/* */
