<?php

/* basel/template/common/language.twig */
class __TwigTemplate_9a50a99781e29587735ce1ba18a7f919148edcc95f2185d3dbb994d91ccfa10e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, (isset($context["languages"]) ? $context["languages"] : null)) > 1)) {
            // line 2
            echo "<div class=\"option\">
<h4>";
            // line 3
            echo (isset($context["text_language"]) ? $context["text_language"] : null);
            echo "</h4>
";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 5
                if (($this->getAttribute($context["language"], "code", array()) == (isset($context["code"]) ? $context["code"] : null))) {
                    // line 6
                    echo "<p><span class=\"anim-underline active\">";
                    echo $this->getAttribute($context["language"], "name", array());
                    echo "</span></p>
";
                } else {
                    // line 8
                    echo "<p><a class=\"anim-underline\" onclick=\"\$('input[name=\\'code\\']').attr('value', '";
                    echo $this->getAttribute($context["language"], "code", array());
                    echo "'); \$('#form-language').submit();\">
";
                    // line 9
                    echo $this->getAttribute($context["language"], "name", array());
                    echo "</a></p>
";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "<select name=\"language-select\" id=\"language-select\" class=\"-hidden-md -hidden-lg\">
";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 14
                if (($this->getAttribute($context["language"], "code", array()) == (isset($context["code"]) ? $context["code"] : null))) {
                    // line 15
                    echo "<option value=\"";
                    echo $this->getAttribute($context["language"], "code", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["language"], "name", array());
                    echo "</option>
";
                } else {
                    // line 17
                    echo "<option value=\"";
                    echo $this->getAttribute($context["language"], "code", array());
                    echo "\">";
                    echo $this->getAttribute($context["language"], "name", array());
                    echo "</option>
";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "</select>
<form action=\"";
            // line 21
            echo (isset($context["action"]) ? $context["action"] : null);
            echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-language\">
<input type=\"hidden\" name=\"code\" id=\"lang-code\" value=\"\" />
<input type=\"hidden\" name=\"redirect\" value=\"";
            // line 23
            echo (isset($context["redirect"]) ? $context["redirect"] : null);
            echo "\" />
</form>
<script>
\$(document).ready(function() {
\$('.mobile-lang-curr').addClass('has-l');
\$('#language-select').appendTo('.mobile-lang-curr');
});
</script>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "basel/template/common/language.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 23,  86 => 21,  83 => 20,  71 => 17,  63 => 15,  61 => 14,  57 => 13,  54 => 12,  45 => 9,  40 => 8,  34 => 6,  32 => 5,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if languages|length > 1 %}*/
/* <div class="option">*/
/* <h4>{{ text_language }}</h4>*/
/* {% for language in languages %}*/
/* {% if language.code == code %}*/
/* <p><span class="anim-underline active">{{ language.name }}</span></p>*/
/* {% else %}*/
/* <p><a class="anim-underline" onclick="$('input[name=\'code\']').attr('value', '{{ language.code }}'); $('#form-language').submit();">*/
/* {{ language.name }}</a></p>*/
/* {% endif %}*/
/* {% endfor %}*/
/* <select name="language-select" id="language-select" class="-hidden-md -hidden-lg">*/
/* {% for language in languages %}*/
/* {% if language.code == code %}*/
/* <option value="{{ language.code }}" selected="selected">{{ language.name }}</option>*/
/* {% else %}*/
/* <option value="{{ language.code }}">{{ language.name }}</option>*/
/* {% endif %}*/
/* {% endfor %}*/
/* </select>*/
/* <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-language">*/
/* <input type="hidden" name="code" id="lang-code" value="" />*/
/* <input type="hidden" name="redirect" value="{{ redirect }}" />*/
/* </form>*/
/* <script>*/
/* $(document).ready(function() {*/
/* $('.mobile-lang-curr').addClass('has-l');*/
/* $('#language-select').appendTo('.mobile-lang-curr');*/
/* });*/
/* </script>*/
/* </div>*/
/* {% endif %}*/
